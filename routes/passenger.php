<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */

//Route::get('bootstrap', 'Api\SettingController@bootstrap')->name("setting.bootstrap");
Route::group(['middleware' => ['ChangeLanguage']], function () {
    Route::post('passenger/signup/otp', 'Api\Passenger\RegistrationController@onlyOTP')->name("passenger.registration.onlyOTP");
    Route::post('passenger/signup', 'Api\Passenger\RegistrationController@register')->name("passenger.registration");
    Route::post('passenger/login', 'Api\Passenger\AuthController@login')->name("passenger.login");
    Route::post('passenger/forget/password', 'Api\Passenger\AuthController@forgetPassword')->name("passenger.forgetPassword");
    Route::post('passenger/reset/password', 'Api\Passenger\AuthController@resetPassword')->name("passenger.resetPassword");
    Route::post('payment/verify/transaction', 'Api\Payment\PayStackController@verifyTransaction')->name("verifyTransaction");
    Route::post('passenger/test','Api\Passenger\RequestController@sendScheduleRideNotification')->name("passenger.test");
    Route::get('passenger/apple-app-site-association', 'Api\Passenger\RequestController@appleAppSiteAssociation')->name("passenger.trip.appleAppSiteAssociation");
    Route::get('passenger/share/ride', 'Api\Passenger\RequestController@rideShareDetailsA')->name("passenger.trip.rideShareDetailsA");
    Route::group(['middleware' => ['auth:api','scope:temporary-customer-service']], function () {
        //Emergency Contact
        Route::post('passenger/emergencyContact', 'Api\Passenger\ProfileController@emergencyContact')->name("passenger.emergencyContact");
        //Mobile no update and verification
        Route::post('passenger/mobile', 'Api\Passenger\ProfileController@updateMobileNo')->name("passenger.mobile");
        Route::put('passenger/mobile/verify', 'Api\Passenger\ProfileController@verifyMobileNo')->name("passenger.mobileVerify");
        Route::get('passenger/mobile/reverify', 'Api\Passenger\ProfileController@resetMobileVerificationOtp')->name("passenger.mobileReVerify");
    });
    Route::group(['middleware' => ['auth:api','scope:passenger-service']], function () {
        //logout
        Route::get('passenger/logout', 'Api\Passenger\AuthController@logout')->name("passenger.logout");
        //profile images update
        Route::post('passenger/profile/image', 'Api\Passenger\ProfileController@profileImageUpdate')->name("passenger.profileImage");
        //profile update  getProfile
        Route::get('passenger/profile', 'Api\Passenger\ProfileController@getProfile')->name("passenger.profile");
        Route::post('passenger/profile/update', 'Api\Passenger\ProfileController@updateProfile')->name("passenger.updateProfile");
        // password
        Route::post('passenger/change/password', 'Api\Passenger\ProfileController@changePassword')->name("passenger.changePassword");
        //leasing
        Route::post('passenger/leasing/daily', 'Api\Passenger\LeasingController@dailyRequest')->name("passenger.leasing.daily");
        Route::post('passenger/leasing/hourly', 'Api\Passenger\LeasingController@hourlyRequest')->name("passenger.leasing.hourly");
        Route::post('passenger/leasing/longtime', 'Api\Passenger\LeasingController@longRequest')->name("passenger.leasing.longtime");
        // background api
        Route::post('passenger/background/data', 'Api\Background\PassengerBackgroundController@get')->name("passenger.backgroundDb");
        //ride request.
        Route::post('passenger/get/estimated/fare','Api\Passenger\RequestController@estimated')->name("passenger.estimatedFare");
        Route::post('passenger/request/ride','Api\Passenger\RequestController@requestRide')->name("passenger.requestRide");
        Route::post('passenger/request/ratingComment','Api\Passenger\RequestController@ratingComment')->name("passenger.ratingComment");
        Route::post('passenger/request/schedule/ride','Api\Passenger\RequestController@scheduleRide')->name("passenger.scheduleRide");
        Route::post('passenger/request/schedule/cancel','Api\Passenger\RequestController@scheduleCancel')->name("passenger.scheduleCancel");
        // trasctions
        Route::post('passenger/transaction/email','Api\Passenger\ProfileController@updateTranscationEmail')->name("passenger.updateTranscationEmail");

        // Route::get('passenger/card', 'Api\Payment\PaymentController@ListCard')->name("passenger.ListCard");
        // Route::post('passenger/card', 'Api\Payment\PaymentController@addCard')->name("passenger.addCard");
        // Route::put('passenger/default/card', 'Api\Payment\PaymentController@setDefaultCard')->name("passenger.setDefaultCard");
        // Route::delete('passenger/card', 'Api\Payment\PaymentController@DeleteCard')->name("passenger.delete");

        /**** Stripe  */
        Route::post('passenger/add/card', 'Api\Passenger\PaymentController@addCard')->name("passenger.addCard");
        Route::get('passenger/card', 'Api\Passenger\PaymentController@ListCard')->name("passenger.ListCard");
        Route::delete('passenger/card', 'Api\Passenger\PaymentController@DeleteCard')->name("passenger.DeleteCard");
        Route::put('passenger/default/card', 'Api\Passenger\PaymentController@setDefaultCard')->name("passenger.setDefaultCard");
        /**** Stripe  */

        Route::get('passenger/get/transaction/list', 'Api\Payment\PaymentController@getCustomerTransaction')->name("passenger.getCustomerTransaction");
        //message
        Route::post('passenger/on/ride/message', 'Api\Driver\MessageController@sendMessage')->name("passenger.trip.sendMessage");
        Route::post('passenger/on/ride/get/message', 'Api\Driver\MessageController@getMessage')->name("passenger.trip.getMessage");
        Route::post('passenger/admin/chat/support', 'Api\Driver\MessageController@sendMessageForSupport')->name("passenger.sendMessageForSupport");
        Route::post('passenger/admin/chat/support/get/message', 'Api\Driver\MessageController@getMessageForSupport')->name("passenger.tripgetMessageForSupport");
        // Trip History
        Route::get('passenger/trip/history', 'Api\Passenger\RequestController@tripHistory')->name("passenger.trip.tripHistory");
        Route::get('passenger/upcoming/schedule', 'Api\Passenger\RequestController@upCommingScheduleTrip')->name("passenger.trip.schedule");
        Route::post('passenger/trip/details', 'Api\Passenger\RequestController@tripDetails')->name("passenger.trip.tripDetails");
        // report issue
         Route::post('passenger/report/issue','Api\ReportIssue\ReportIssueController@reportIssue')->name('passenger.report.issue');
        Route::get('report/issue/passenger/subjects','Api\ReportIssue\ReportIssueController@reportIssuePassengerSubjectList')->name('report.issue.passenger.subjects');
        
        Route::get('passenger/ride/share', 'Api\Passenger\RequestController@rideShareDetails')->name("passenger.trip.rideShareDetails");
        // promocode
        Route::post('passenger/add/promo', 'Api\Promocode\PromocodeController@addPromoCode')->name("passenger.addPromoCode");
        Route::get('passenger/get/promo', 'Api\Promocode\PromocodeController@getPromoCode')->name("passenger.getPromoCode");
        // ride cancelation
        Route::post('passenger/ride/cancel', 'Api\Passenger\RequestController@cancelRide')->name("passenger.cancelRide");

        // update langage seletion
        Route::put('passenger/update/lang', 'Api\Passenger\ProfileController@updateLang')->name("passenger.lang");

        Route::post('passenger/sos/email', 'Api\TermCondition\TermConditionController@sosEmail')->name("passenger.sos.email");

    });
});
