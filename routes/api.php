<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/* Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
}); */
Route::group(['middleware' => ['ChangeLanguage']], function () {
    Route::get('get/service/list', 'Api\ServiceTypeMstController@get')->name("get.servicemst");
    Route::get('get/service/leasing/list', 'Api\ServiceTypeMstController@getForLeasingList')->name("get.servicemst.leasinglist");
    Route::get('get/model/list', 'Api\VehicleMst\VehicleMstController@get')->name("get.servicemst");
    Route::get('get/service/make', 'Api\VehicleMst\VehicleMstController@getMake')->name("get.servicemake");
    Route::post('get/service/model', 'Api\VehicleMst\VehicleMstController@getModel')->name("get.servicemodel");
    Route::post('get/service/year', 'Api\VehicleMst\VehicleMstController@getYear')->name("get.serviceyear");
    Route::put('transaction/emailverify', 'Api\Payment\PaymentController@verifyEmailID')->name("transaction.emailverify");
    //get passenger T&C
    Route::post('get/terms/conditions', 'Api\TermCondition\TermConditionController@getTermCondition')->name("get.terms.conditions");
    //get driver T&C
    Route::post('get/terms/conditions/driver', 'Api\TermCondition\TermConditionController@getTermConditionDriver')->name("get.terms.conditions.driver");

    //get passenger's privacy policy
    Route::post('get/privacy/policy', 'Api\TermCondition\TermConditionController@getPrivacyPolicy')->name("get.privacy.policy");
    //get driver's privacy policy
    Route::post('get/privacy/policy/driver', 'Api\TermCondition\TermConditionController@getPrivacyPolicyDriver')->name("get.privacy.policy.driver");

    Route::post('contactus', 'Api\TermCondition\TermConditionController@contactUS')->name("contactus");

});
