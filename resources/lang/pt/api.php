<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'failed' => 'Essas credenciais não correspondem aos nossos registros.',
    'throttle' => 'Muitas tentativas de login. Tente novamente em: segundos segundos.',
    'notAuthorize'=>'Você não está autorizado a acessar esta página',
    'SYSTEM_MESSAGE'=>[
        'DRIVER_CREATE'=>'Conta do motorista criada com sucesso.',
        'LOGGED_IN'=>'Logado!',
        'DOCUMENT_UPDATED'=>'Documento atualizado com sucesso',
        'DOCUMENT_LIST'=>'Lista de documentos',
        'MESSAGE_SENT'=>'Mensagem enviada!',
        'OLD_PASSWORD_NOT_MATCH'=>'Sua senha antiga não corresponde à senha que você forneceu. Por favor, tente novamente',
        'NEW_PASSWORD_SAME_AS_OLD'=>'A nova senha não pode ser igual à senha antiga. Escolha uma senha diferente',
        'PROVIDE_VALID_PHONE_NUMBER'=>'Forneça um número de telefone válido.',
        'PHONE_NUMBER_VERIFED'=>'Obrigado por verificar seu telefone não conosco!',
        'PHONE_NUMBER_NOT_VERIFED'=>'Por favor, verifique o seu número de telefone.',
        'REGISTRATION_FAILED'=>'O registro não é possível. Entre em contato com o administrador.',
        'WAIT_FOR_CUSTOMER'=>'Por favor espere pelo cliente',
        'REQUEST_DECLINE'=>'Você recusou, um pedido',
        'REQUEST_ACCEPTED'=>'Obrigado por aceitar o pedido, prossiga para o local de recolha.',
        'PROCEED_TO_DESTINATION'=>'Por favor, prossiga em direção ao destino.',
        'PAYMENT_COLLECTED'=>'Obrigado por receber o pagamento',
        'CALCULATED_PAYABLE'=>'Por favor, espere um pouco, enquanto calculamos a tarifa a pagar',
        'RATING_CUSTOMER'=>'Obrigado por avaliar o cliente.',
        "ON_RIDE"=>"Parece que você já está viajando. A solicitação de cancelamento de sua viagem não pode ser processada.",
        'INVALID_LANGUAGE_SELECTION'=>'Seleção de idioma inválida',
        'VALIDATION_FAILED'=>'Falha na validação do pedido',
        'SOMETHING_WENT_WRONG'=>'Algo deu errado',
        'HTTP_BAD_REQUEST'=>'Exceção de resposta Http: solicitação inválida',
        'DATABASE_EXCEPTION'=>'Exceção de banco de dados',
        'WORKING_AS_EXPECTED'=>'está funcionando como esperado',
        'SERVICE_LIST'=>'Lista de serviços',
        'EMAIL_NOT_FOUND'=>'Id de email não encontrado.',
        'EMAIL_NOT_VERIFIED'=>'ID de e-mail não verificado.',
        'EMAIL_FOUND'=>'Id de e-mail encontrado.',
        'EMAIL_VERIFIED'=>'ID do e-mail verificado.',
        '404_ERROR'=>'Sem conteúdo',
        'INTERNAL_SERVER_ERROR'=>'Erro do Servidor Interno',
        'MAKE_FOUND'=>'Lista de fabricantes de automóveis',
        'MAKE_NOT_FOUND'=>'Fabricantes de automóveis não encontrados.',
        'MODEL_FOUND'=>'Lista de modelos de carros',
        'MODEL_NOT_FOUND'=>'Modelo do carro não encontrado.',
        'MODEL_YEAR_FOUND'=>'Lista de anos do modelo do carro',
        'MODEL_YEAR_NOT_FOUND'=>'Ano não encontrado.',
        'verifying_email_ID'=>'Obrigado por verificar seu ID de e-mail',
        'Everything_OK'=>'Tudo ok',
        'Not_Found_Exception'=>'Exceção de modelo não encontrado',
        'Password_successfully_Changed'=>'A senha da sua conta foi alterada com sucesso',
        'Not_Registered'=>'Você não está registrado conosco!',
        'Exception_From_Twilo'=>'Exceção de Twilo',
        'OTP_Sent'=>'OTP enviado com sucesso!',
        'System_Error'=>'Erro no sistema!',
        'Resoures_Created'=>'Recursos Criados',
        'Promo_Code_Not_Exit'=>'O código promocional não sai',
        'Promo_Code_Not_Exit_Redeemed.'=>'O código promocional que você inseriu já foi resgatado.',
        'Promo_Code_Added'=>'Código promocional adicionado',
        'Promo_Code_Available'=>'Código promocional disponível',
        'List_Promo_Codes'=>'Lista de códigos promocionais',
        'Card_List'=>'Lista de Cartas',
        'No_Card_Found'=>'Nenhum cartão encontrado. Adicione um cartão.',
        'Cannot_Fetch_Card'=>'Não é possível buscar seu cartão. Por favor tente mais tarde',
        'Card_Added'=>'Cartão adicionado',
        'Cannot_Add_Card!'=>'Não é possível adicionar cartão!',
        'Default_Card_Updated'=>'Cartão padrão atualizado',
        'Cannot_Update_Card'=>'Não é possível atualizar seu cartão. Por favor tente mais tarde',
        'No_Card_Found.'=>'Nenhum cartão encontrado.',
        'Card deleted'=>'Cartão excluído',
        'cannot_delete_card'=>'Não é possível excluir seu cartão. Por favor tente mais tarde',
        'cannot_make_payment_from_card'=>'Não é possível fazer o pagamento com seu cartão. Por favor tente mais tarde',
        'profile_updated'=>'Seu perfil foi atualizado com sucesso',
        'profile_cannot_updated!'=>'Seu perfil não pode ser atualizado!',
        'otp_for_mobile_number_verification'=>'Sua senha única para a verificação do número do celular é: OTP',
        'thank_you_verifying_mobile_number'=>'Obrigado por verificar o seu número de celular',
        'sorry_cannot_verify_mobile_number'=>'Desculpe, não posso verificar o seu número de celular',
        'thank_you_uploading_profile_image!'=>'Obrigado por enviar sua imagem de perfil!',
        'cars_available_your_location'=>'Nenhum carro está disponível para sua localização naquele momento.',
        'no_cars_available_your_location'=>'Nenhum carro está disponível para sua localização naquele momento.',
        'services_added'=>'Serviços Adicionados',
        'mobile_number_password_is_incorrect'=>'O número do celular ou a senha inserida está incorreta',
        'incorrect_method_authentication'=>'Método de autenticação incorreto.',
        'logout_successfully'=>'Você fez o logout com sucesso.',
        'user_not_found'=>'Usuário não encontrado',
        'you_registered_using_social_platform'=>'você se cadastrou usando a plataforma social. Redefina sua senha na respectiva plataforma social.',
        'no_mobile_number_found'=>'Nenhum número de celular encontrado',
        'otp_to_reset_password'=>'Sua senha única para redefinir a senha da sua conta DIA é: OTP',
        'email_text_verification'=>'Verifique sua conta clicando no seguinte link / botão de verificação:',
        'email_text_application_thanks'=>'Obrigado por usar nosso aplicativo!',
        'email_otp_for_resetpassword'=>'Você está recebendo este e-mail porque recebemos uma solicitação de redefinição de senha de sua conta. Seu OTP é',
        'email_otp_for_resetpassword_message'=>'Se você não solicitou uma redefinição de senha, nenhuma ação adicional será necessária.',
        'no_driver_available'=>'Desculpe, nenhum driver está disponível no momento. Por favor, tente depois de algum tempo.',
        'connecting_with_drivers'=>'Conectando-se com os motoristas próximos.',
        
        'verification_mail'=>'Um e-mail de verificação foi enviado para o seu ID de e-mail',
        'thank_message'=>'Obrigado por nos escolher, estamos nos conectando com os motoristas próximos.',
        'appraisal_message'=>'Estamos entrando em contato com os motoristas próximos, agradecemos sua paciência.',
        'on_date_time'=>'Obrigado por nos escolher, os motoristas próximos entrarão em contato com você na data e hora programadas',
        'schedule_cancel'=>'Sua programação foi cancelada com sucesso.',
        'issue_reported'=>'Seu problema foi relatado. Entraremos em contato em breve',
        'DEFAUTL_CARD'=>'Cartão padrão atualizado',
        'CARD_DELETE'=>'Cartão excluído',
        'CARDADDED'=>'Cartão já adicionado',
        'CARDADD'=>'Cartão adicionado',
        'NOCARDADD'=>'Nenhum cartão encontrado. Adicione um cartão.',
        'SOSEMAIL'=>'E-mail enviado com sucesso.',
        'STRIPE_ACCOUNT_NOT_FOUND'=>'Conta do Stripe não encontrada.',
        'STRIPE_ACCOUNT_NOT_CREATE'=>'País não compatível. Não é possível criar uma conta!',
        'REQUESTED_CAPABILITIES'=>'Você precisa solicitar recursos para a conta conectada.',
        'PAST_DUE'=>'Informações adicionais de verificação são necessárias para habilitar os recursos de pagamento ou cobrança nesta conta.',
        'PENDING_VERIFICATION'=>'Stripe está atualmente verificando informações sobre a conta conectada.',
        'PROHIBITED'=>'A conta pode estar em uma lista de pessoas ou empresas proibidas (Stripe investigará e rejeitará ou restabelecerá a conta apropriadamente).',
        'UNDER_REVIEW'=>'Conta está sob revisão por Stripe.',
        'DISABLED'=>'A conta não foi rejeitada, mas foi desativada por outro motivo durante a revisão.',
        'PENDING'=>'Informações adicionais necessárias.',
        'ACTIVE'=>'Sua conta está ativa.'

    ],
    'NOTIFICATION'=>[
        'TITLE'=>[
            'RIDE_REQUEST'=>'Pedido de viagem',
            'DRIVER_CHAT'=>'Mensagem do motorista',
            'PASSENGER_CHAT'=>'Mensagem do Passageiro',
            'ADMIN_CHAT'=>'Mensagem de Admin',
            'RIDE_ACCEPTED'=>'Boléia aceite',
            'RIDE_REACHED'=>'Seu motorista / sua boléia chegou',
            'RIDE_COMPLETED'=>'Viagem Concluída',
            'REQUEST_CANCLED'=>' Solicitação de viagem cancelada',
            'SCHEDULED_RIDE_CANCEL'=>'Viagem programada cancelada',

        ],
        'MESSAGE'=>[
            'RIDE_REQUEST'=>'Você tem uma nova solicitação de viagem.',
            'RIDE_ACCEPTED'=>'O motorista aceitou sua solicitação de viagem, ele chegará em seu  local de coleta em breve.',
            'RIDE_REACHED'=>'O motorista chegou ao seu local de coleta.',
            'RIDE_COMPLETED'=>'Obrigado por escolher o DIA. Por favor, ligue novamente.',
            'REQUEST_CANCEL_MESSAGE_DRIVER_TO_PASSENGER'=>'O pedido de viagem é cancelado pelo motorista.',
            'REQUEST_CANCEL_MESSAGE_PASSENGER_TO_DRIVER'=>'A solicitação de viagem foi cancelada pelo passageiro.',
            'REQUEST_CANCEL_BY_DRIVER'=>'Solicitação de viagem cancelada por você. A taxa de cancelamento de: cancelar Valor: a moeda será deduzida de sua carteira.',
            'REQUEST_CANCEL_BY_PASSENGER'=>'Sua Sua solicitação foi cancelada.',
            'REQUEST_CANCEL_BY_PASSENGER_WITH_FEE'=>'Sua  solicitação foi cancelada e: cancelar Valor: a moeda será cobrada em sua próxima viagem como taxa de cancelamento',
            'SCHEDULED_RIDE_CANCEL'=>'Sua viagem agendada, que deve chegar nos próximos 10 minutos, foi cancelada por nós porque você já está em uma viagem.',
            ]
        ],

        'MESSAGE'=>[

        ]

    ];
