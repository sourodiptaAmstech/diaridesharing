<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="author" content="">
    <script type="text/javascript" src="{{asset('main/vendor/jquery/jquery-1.12.3.min.js')}}"></script>
</head>
<body>
    <script>
        var user_id = {{$user_id}};
        $.ajax({
            url: "{{ route('driver.refresh.stripe.account') }}",
            method: 'GET',
            data: { user_id: user_id },
            success: function(response){
                location.href=response.data.url;
            },
            error: function(response){
                console.log(response);
            }
        });
    </script>
</body>
</html>