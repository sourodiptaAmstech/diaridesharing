@extends('admin.layout.base')

@section('title', 'Update Subject')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.reportIssue.driver.subject.list') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Subject</h5>

            <form class="form-horizontal" action="{{route('admin.reportIssue.driver.subject.update', $ReportSubject->report_subject_id )}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PUT">
                <div class="form-group row">
                    <label for="subject" class="col-xs-2 col-form-label">Subject Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $ReportSubject->subject }}" name="subject" required id="subject" placeholder="Subject Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="subject_pt" class="col-xs-2 col-form-label">Subject Name(Portuguese)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $ReportSubject->subject_pt }}" name="subject_pt" required id="subject_pt" placeholder="Subject Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Update Subject</button>
                        <a href="{{route('admin.reportIssue.driver.subject.list')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

@endsection
