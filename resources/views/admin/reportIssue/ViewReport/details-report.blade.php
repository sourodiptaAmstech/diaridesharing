@extends('admin.layout.base')

@section('title', 'Report Details')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            
            @if($param=='driver')
            <a href="{{ route('admin.reportIssue.driver.list') }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='customer')
            <a href="{{ route('admin.reportIssue.customer.list') }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            
            <h4>Report Details</h4>
            <br>
            <div class="row">
                <div class="col-md-6">
                    <dl class="row">
                        @if($param=='customer')
                            @if(isset($ReportIssue->customer))

                            @if(isset($ReportIssue->customer->picture))
                            <div class="col-sm-12 profile-image position-relative my-2 d-flex justify-content-center align-items-center" >
                                <img src="{{$ReportIssue->customer->picture}}" alt="Profile" id="promap_image" style="width: 30%;height: auto;">
                            </div>
                            @else
                            <div class="col-sm-12 profile-image position-relative my-2 d-flex justify-content-center align-items-center" >
                                <img src="{{URL::asset('/')}}asset/img/profile.png" alt="Profile" id="promap_image" style="width: 30%;height: auto;">
                            </div>
                            @endif

                            <dt class="col-sm-4">Passenger Name :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->customer->first_name }} {{ $ReportIssue->customer->last_name }} </dd>

                            <dt class="col-sm-4">Passenger's Mobile No. :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->customer->isd_code }}-{{ $ReportIssue->customer->mobile_no }} </dd>
                            @else
                            @endif

                            <dt class="col-sm-4">Subject :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->reportSubject->subject }} </dd>
                            <dt class="col-sm-4">Description :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->description }} </dd>
                        @endif
                        @if($param=='driver')
                            @if(isset($ReportIssue->driver))

                            @if(isset($ReportIssue->driver->picture))
                            <div class="col-sm-12 profile-image position-relative my-2 d-flex justify-content-center align-items-center" >
                                <img src="{{$ReportIssue->driver->picture}}" alt="Profile" id="promap_image" style="width: 30%;height: auto;">
                            </div>
                            @else
                            <div class="col-sm-12 profile-image position-relative my-2 d-flex justify-content-center align-items-center" >
                                <img src="{{URL::asset('/')}}asset/img/profile.png" alt="Profile" id="promap_image" style="width: 30%;height: auto;">
                            </div>
                            @endif

                            <dt class="col-sm-4">Driver Name :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->driver->first_name }} {{ $ReportIssue->driver->last_name }} </dd>

                            <dt class="col-sm-4">Driver's Mobile No. :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->driver->isd_code }}-{{ $ReportIssue->driver->mobile_no }} </dd>
                            @else
                            @endif

                            <dt class="col-sm-4">Subject :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->reportSubject->subject }} </dd>
                            <dt class="col-sm-4">Description :</dt>
                            <dd class="col-sm-8">{{ $ReportIssue->description }} </dd>
                        @endif

                    </dl>
                </div>
                <div class="col-md-6">
                    <div id="map"></div>
                </div>
            </div>
        </div>

    </div>
</div>

@endsection
