@extends('admin.layout.base')

@section('title', 'Add Driver ')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
    	    <a href="{{ route('admin.driver.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

			<h5 style="margin-bottom: 2em;">Add Driver</h5>

            <form class="form-horizontal" action="{{route('admin.driver.store' )}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
            	
				<div class="form-group row">
					<label for="first_name" class="col-xs-2 col-form-label">First Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="first_name" value="{{ old('first_name') }}" id="first_name" placeholder="First Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="last_name" class="col-xs-2 col-form-label">Last Name</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="last_name" value="{{ old('last_name') }}" id="last_name" placeholder="Last Name" required>
					</div>
				</div>

				<div class="form-group row">
					<label for="email" class="col-xs-2 col-form-label">Email</label>
					<div class="col-xs-10">
						<input class="form-control" type="email" name="email" value="{{ old('email') }}" id="email" placeholder="Email">
					</div>
				</div>

				<div class="form-group row">
					<label for="picture" class="col-xs-2 col-form-label">Picture</label>
					<div class="col-xs-10">
						<input type="file" accept="image/*" name="picture" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
					</div>
				</div>

				<div class="form-group row">
					<label for="mobile" class="col-xs-2 col-form-label">Mobile</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="mobile" id="mobile" placeholder="Mobile No" required>
						<input type="hidden" name="code" id="code">
					</div>
				</div>

				<div class="form-group row">
					<label for="dob" class="col-xs-2 col-form-label">Date of Birth</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="dob" value="{{ old('dob') }}" id="dob">
					</div>
				</div>
				<div class="form-group row">
					<label for="gender" class="col-xs-2 col-form-label">Gender</label>
					<div class="col-xs-10">
						<input type="radio" id="Male" name="gender" value="Male">
						<label for="Male">Male</label><br>
						<input type="radio" id="Female" name="gender" value="Female">
						<label for="Female">Female</label>
					</div>
				</div>

				<div class="form-group row">
					<label for="service_type_id" class="col-xs-2 col-form-label">Service Type</label>
					<div class="col-xs-10">
						<select name="service_type_id" id="service_type_id" class="form-control" required>
						<option value="">Select Service Type</option>
						@foreach($ServiceTypeMstDatas as $ServiceTypeMstData)
							<option value="{{$ServiceTypeMstData['id']}}">{{$ServiceTypeMstData['name']}}</option>
						@endforeach
						</select>
					</div>
				</div>

				<div class="form-group row">
					<label for="car_make" class="col-xs-2 col-form-label">Car Make</label>
					<div class="col-xs-10">
						<select name="car_make" id="car_make" class="form-control" required>
						
						</select>
					</div>
				</div>
				<div class="form-group row">
					<label for="car_model" class="col-xs-2 col-form-label">Car Model</label>
					<div class="col-xs-10">
						<select name="car_model" id="car_model" class="form-control" required>
						
						</select>
						{{-- <input type="text" name="car_model" id="car_model" class="form-control" placeholder="Car Model" readonly> --}}
					</div>
				</div>
				<div class="form-group row">
					<label for="model_year" class="col-xs-2 col-form-label"> Model Year</label>
					<div class="col-xs-10">
						<select name="model_year" id="model_year" class="form-control" required>
						
						</select>
						{{-- <input type="text" name="model_year" id="model_year" class="form-control" placeholder="Model Year" readonly> --}}
					</div>
				</div>
				 
				<div class="form-group row">
					<label for="car_number" class="col-xs-2 col-form-label">Car Number</label>
					<div class="col-xs-10">
						<input type="text" name="car_number" id="car_number" class="form-control" placeholder="Car Number" value="{{ old('car_number') }}" required>
					</div>
				</div>

				<div class="form-group row password">
					<label for="password" class="col-xs-2 col-form-label">Password</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password" id="password" placeholder="Password" value="{{ old('password') }}" required>
					</div>
				</div>
				<div class="form-group row">
					<label for="password_confirmation" class="col-xs-2 col-form-label">Password Confirmation</label>
					<div class="col-xs-10">
						<input class="form-control" type="password" name="password_confirmation" value="{{ old('password_confirmation') }}" id="password_confirmation" placeholder="Re-type Password">
					</div>
				</div>


				{{-- <div class="form-group row">
					<label for="car_plate_number" class="col-xs-2 col-form-label">Car Plate Number</label>
					<div class="col-xs-10">
						<input class="form-control" type="text" name="car_plate_number" value="{{ old('car_plate_number') }}" id="car_plate_number" placeholder="Car Plate Number" required>
					</div>
				</div> --}}
				{{-- <div class="form-group row">
					<label for="car_number_expire_date" class="col-xs-2 col-form-label">Car Number Plate Expiry Date</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" name="car_number_expire_date" value="{{ old('car_number_expire_date') }}" id="car_number_expire_date" placeholder="Car Number Expire Date" required>
					</div>
				</div> --}}

				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Driver</button>
						<a href="{{route('admin.driver.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form>
		</div>
    </div>
</div>
<style type="text/css">.iti { width: 100%; }</style>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
 <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/css/intlTelInput.css">
 <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/intl-tel-input/16.0.15/js/intlTelInput-jquery.min.js"></script>
 <script>
	jq = jQuery.noConflict();

    use :
   		var s = jq("#mobile").intlTelInput({
			autoPlaceholder: 'polite',
   			separateDialCode: true,
   			formatOnDisplay: true,
   			initialCountry: 'ng',
   			preferredCountries:["ng"]
   		});

   	insteadof :
   		var d = jq("#mobile").intlTelInput("getSelectedCountryData").dialCode;
   		jq("#code").val(d);
		jq(document).on('countrychange', function (e, countryData) {
        	jq("#code").val((jq("#mobile").intlTelInput("getSelectedCountryData").dialCode));
    	});
</script>

@endsection
@section('scripts')
<script>
  	$(document).ready(function() {
	    const servicetype = '{{ old('service_type_id') }}';
	    if(servicetype !== '') {
	      $('#service_type_id').val(servicetype);
	    }

	$('#car_make').change(function(e){
		e.preventDefault()
        var make = $(this).val();
        $.ajaxSetup({
            headers:{
              'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        // $(".overlay").show();
        $.ajax({
            url: "{{ route('admin.get.admin.model') }}",
            method: "POST",
            data: {make: make},
            success: function(response){
                // $(".overlay").hide();
                $('#car_model').empty();
                var options = '<option value="">Choose Car Model</option>';
                $.each(response.data, function(key, value) {
                    options += '<option style="color:#080600;" value="'+value.model+'">'+value.model+'</option>';
                });
                $('#car_model').append(options);
                
                $('#model_year').html('');
            },
            error: function(response){
                // $(".overlay").hide();
                if (response.status == 422){
                    $('#car_model').html('');
                    $('#model_year').html('');
                } 
            }
        });
    });


    $('#car_model').change(function(){
        var model = $(this).val();
        var make = $('#car_make').val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        //$(".overlay").show();
        $.ajax({
            url: "{{ route('admin.get.admin.year') }}",
            method: "POST",
            data: {make: make, model: model},
            success: function(response){
                //$(".overlay").hide();
                $('#model_year').empty();
                var options = '<option value="">Choose Model Year</option>';
                $.each(response.data, function(key, value) {
                    options += '<option style="color:#080600;" value="'+value.year+'">'+value.year+'</option>';
                });
                $('#model_year').append(options);
            },
            error: function(response){
                //$(".overlay").hide();
                if (response.status == 422){
                    $('#model_year').html('');
                }
            }
        });
    });

    $.ajax({
        url: "{{ route('admin.get.admin.make') }}",
        method: "GET",
        data: {},
        success: function(response){
            console.log(response);
            $('#car_make').empty();
            var options = '<option value="">Choose Car Make</option>';
            $.each(response.data, function(key, value) {
              options += '<option style="color:#080600;" value="'+value.make+'">'+value.make+'</option>';
            });
            $('#car_make').append(options);
        },
        error: function(response){
            console.log(response);
        }
    });

  	});
</script>
@endsection