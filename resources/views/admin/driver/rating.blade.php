@extends('admin.layout.base')

@section('title', 'Driver Rating ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Driver Rating
            </h5>
            <h5 class="float-left pull-left">
                @if(isset($ratings))
                Driver Name: {{$ratings[0]->userDriver->first_name}} {{$ratings[0]->userDriver->last_name}}
                @endif
            </h5>
            <a href="{{ route('admin.driver.rating-alert', $ratings[0]->driver_id) }}" style="margin-left: 1em;" data-id="{{$ratings[0]->driver_id}}" class="btn btn-warning driver_alert pull-right"> Driver Alert</a>

            <div class="row col-lg-12" style="margin-bottom: 5px;">
               {{--  <div class="col-lg-6">
                Search <select name="search" id="search">
                        <option value="last7rides">Last 7 rides</option>
                        <option value="last10rides">Last 10 rides</option>
                        <option value="last15rides">Last 15 rides</option>
                        <option value="last20rides">Last 20 rides</option>
                    </select>
                </div> --}}
            </div>

            <table class="table table-striped table-bordered dataTable">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Rating</th>
                        <th>Remarks</th>
                    </tr>
                </thead>
                <tbody id="tBody">
                    @foreach($ratings as $index => $rating)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $rating->driver_rating }}</td>
                        <td>{{ $rating->driver_remarks }}</td>
                    </tr>
                    @endforeach
                </tbody>

            </table>
            <h4>Average Rating: <span class="avg">{{round($avg_rating,2)}}</span></h4>
        </div>
    </div>
</div>
{{-- ..........Document Modal................. --}}
<div class="modal fade" id="driverAlertModel" data-backdrop="static" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-md modal-dialog-centered" role="document">
    <div class="modal-content" style="background-color: lavender;">
      <div class="modal-header">
        <h5 class="modal-title"></h5>
      </div>
      <div class="modal-body">
      
        <div class="d-flex justify-content-center" >
            <div class="col-lg-12">

                <div class="alert alert-danger rating-danger" role="alert" style="display: none; height: 42px;">
                    <p class="rating-error" style="color: red; padding: 0px;"></p>
                </div>

                <form>
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="form-group">
                                <label class="control-label text-dark">Email Body</label>
                                <textarea name="email_body" id="email_body" rows="4" class="form-control" required></textarea>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12">
                            <button type="submit" class="btn btn-info float-left" id="store_driver_alert" style="width: 70px;">
                                <span id="msg-submit">Submit</span>
                                <i id="spin-submit" class="fa fa-circle-o-notch fa-spin" style="font-size: 20px; display: none;"></i>
                            </button>
                            <a href="" class="btn btn-secondary float-right">Close</a>
                        </div>
                    </div>
                </form>
                 
            </div>
        </div>
      </div>
      <div class="modal-footer">
      </div>
    </div>
  </div>
</div>
{{-- end Document Modal --}}

@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $('#search').change(function(){
            var value = $(this).val();
            var id = {{$ratings[0]->driver_id}};
            $.ajax({
                url: '{{ route('admin.driver.rating-search',0) }}',
                method: 'GET',
                data: { value:value, id:id },
                success: function(response){
                    console.log(response.ratings.length);
                    $('.avg').text(response.avg_rating);
                    $('#tBody').empty();
                    var trHTML = '';
                    for (var i = 0; i < response.ratings.length; i++) {
                        trHTML +=
                            '<tr><td>'
                            + (i+1)
                            + '</td><td>'
                            + response.ratings[i].driver_rating
                            + '</td><td>'
                            + response.ratings[i].driver_remarks 
                            + '</td></tr>';
                    }
                    $('#tBody').append(trHTML);
                }
            });
        });

        $(document).on('click','.driver_alert', function(e){
            e.preventDefault();
            var id = $(this).data("id");
            $('#driverAlertModel').modal('show');
            $('.modal-title').html('Send alert message to driver for low rating');

            $('#store_driver_alert').click(function(e){
              e.preventDefault()
              $.ajaxSetup({
                headers:{
                  'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
              });
              var email_body = $('#email_body').val();
              var avg_rating = {{round($avg_rating,2)}};
              var last_id = {{$last_id}};
              var first_id = {{$first_id}};
              $('#msg-submit').css({'display':'none'});
              $('#spin-submit').css({'display':'block'});
              $.ajax({
                url: '{{ route('admin.driver.rating-alert',[0]) }}',
                method: 'post',
                data: { id: id, email_body: email_body, avg_rating: avg_rating, last_id: last_id, first_id: first_id },
                success: function(response){
                    if (response.success) {
                        // $('#msg-submit').css({'display':'block'});
                        // $('#spin-submit').css({'display':'none'});
                        // $('.rating-danger').hide();
                        // $('#email_body').val('');
                        // $('#driverAlertModel').modal('hide');
                        location.reload();
                        //$('.success_alert').show();
                        //$('.success_msg').html(response.message);
                        //$('#driverAlertModel').dialog("close");
                    }
                },
                error: function(response){
                  $('#msg-submit').css({'display':'block'});
                  $('#spin-submit').css({'display':'none'});
                  $('.rating-danger').show();
                  $('.rating-error').html(response.responseJSON.errors.email_body);
                }
              });

            });
           
        });
    });
</script>
@endsection