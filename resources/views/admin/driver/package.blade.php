@extends('admin.layout.base')

@section('title', 'Package')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">
                Package Details
            </h5>
            <a href="{{ route('admin.driver.index') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-angle-left"></i> Back</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>No. of Credit</th>
                        <th>Credit Remaining</th>
                        <th>Transaction ID</th>
                        <th>Transaction Status</th>
                        <th>Package Cost($)</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($transaction['data']['packageDetails'] as $index => $driver)
                    <tr>
                        <td>{{$index + 1}}</td>
                        <td>{{$driver['name']}}</td>
                        <td>{{$driver['face_value']}}</td>
                        <td>{{$driver['remaining_credit']}}</td>
                        <td>{{$driver['transaction_id']}}</td>
                        <td>{{$driver['transaction_status']}}</td>
                        <td>{{$driver['transaction_cost']}}</td>
                    </tr>
                    @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Name</th>
                        <th>No. of Credit</th>
                        <th>Credit Remaining</th>
                        <th>Transaction ID</th>
                        <th>Transaction Status</th>
                        <th>Package Cost($)</th>
                    </tr>
                </tfoot>
            </table>
        </div>

        <div class="row row-md mb-2">
        <div class="col-md-12">
                <div class="box bg-white">
                    <div class="box-block clearfix">
                        <h5 class="float-xs-left">Summary</h5>
                    </div>
                    <table class="table mb-md-0 table-bordered table-hover">
                        <tbody>
                            <tr>
                                <th scope="row">Total Credit</th>
                                <td>
                                    {{$transaction['data']['totalDebitCredit']['TotalCredit_BitCredit']}}
                                </td>
                            </tr>
                            <tr>
                                <th scope="row">Total Debit</th>
                                <td>
                                    {{$transaction['data']['totalDebitCredit']['TotalDebit_BitCredit']}}
                                </td>
                            </tr>

                            <tr>
                                <th scope="row">Remaining Credit</th>
                                <td>
                                    {{$transaction['data']['totalDebitCredit']['remaining_credit']}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection
