@extends('admin.layout.base')

@section('title', 'Chat Support')

@section('content')

<div class="content-area py-1">
<div class="container-fluid">

{{-- <div class="container app"> --}}
  <div class="row app-one">
    <div class="col-sm-4 side">
      <div class="side-one">
        <div class="row heading">
          <div class="col-sm-12 col-xs-12 heading-avatar">
            <h4>Chat With Driver</h4>
            {{-- <div class="heading-avatar-icon">
              <img src="https://bootdey.com/img/Content/avatar/avatar1.png">
            </div> --}}
          </div>
        </div>

        <div class="row searchBox">
          <div class="col-sm-12 searchBox-inner">
            <div class="form-group has-feedback">
              <input id="searchText" type="text" class="form-control" name="searchText" placeholder="Search" autocomplete="off">
              <span class="glyphicon glyphicon-search form-control-feedback"></span>
            </div>
          </div>
        </div>

        <div class="row sideBar">

         @foreach($drivers as $driver)
          <div class="row sideBar-body driver_user" id="{{$driver->user_id}}">
            <div class="col-sm-3 col-xs-3 sideBar-avatar">
              <div class="avatar-icon">
                @if(isset($driver->picture))
                  <img src="{{$driver->picture}}">
                @else
                  <img src="{{URL::asset('/')}}asset/img/profile.png">
                @endif
              </div>
            </div>
            <div class="col-sm-9 col-xs-9 sideBar-main">
              <div class="row">
                <div class="col-sm-8 col-xs-8 sideBar-name">
                  <span class="name-meta">{{$driver->first_name}} {{$driver->last_name}}
                </span>
                </div>
                {{-- <input type="hidden" name="user_id" id="user_id" value="{{$driver->user_id}}"> --}}
                <div class="col-sm-4 col-xs-4 pull-right sideBar-time">
                  <span class="time-meta pull-right">
                </span>
                </div>
              </div>
            </div>
          </div>
          @endforeach

        </div>

      </div>


    </div>



    <div class="col-sm-8 conversation">
      <div class="row heading">
        <div class="col-sm-2 col-md-1 col-xs-3 heading-avatar">
          <div class="heading-avatar-icon">
            <img src="{{URL::asset('/')}}asset/img/profile.png" id="driver_img">
          </div>
        </div>
        <div class="col-sm-8 col-xs-7 heading-name">
          <a class="heading-name-meta" id="driver_name">
          </a>
          <span class="heading-online">Online</span>
        </div>
      {{--   <div class="col-sm-1 col-xs-1  heading-dot pull-right">
          <i class="fa fa-ellipsis-v fa-2x  pull-right" aria-hidden="true"></i>
        </div> --}}
      </div>


      <div class="row message" id="conversation">

       {{--  <div class="row message-previous">
          <div class="col-sm-12 previous">
            <a onclick="previous(this)" id="ankitjain28" name="20">
            Show Previous Message!
            </a>
          </div>
        </div> --}}

      </div>


      <div class="row reply">
        <form id="messageForm">
          <div class="col-sm-12 col-xs-12 reply-main">
            <textarea class="form-control" rows="1" id="message" required onkeyup="return validate()" placeholder="Write a message..." autocomplete="off"></textarea>
            <input type="hidden" name="thread_id" id="thread_id">
          </div>
          
          <button type="submit" class="send-message-button btn-info"> <i class="fa fa-send"></i> </button>
        
        </form>
      </div>

    </div>
  </div>
{{-- </div> --}}

<style>
  .glyphicon {
    float: right;
    margin-top: -23px;
    margin-right: 5px;
    }
  .send-message-button {
      right: 40px;
    }
  .send-message-button {
    display: block;
    position: absolute;
    width: 35px;
    height: 35px;
    right: 100px;
    top: 464px;
    bottom: 0;
    margin: auto;
    border: 1px solid rgba(0,0,0,0.05);
    outline: none;
    font-weight: 600;
    border-radius: 50%;
    padding: 0;
  }

  .send-message-button > i {
    font-size: 16px;
    margin: 0 0 0 -2px;
  }
  .send-message-button {
    right: 35px;
  }
  /*.upload-file {
    right: 85px;
  }
  .upload-file input[type="file"] {
    position: fixed;
    top: -1000px;
  }
  .upload-file {
    display: block;
    position: absolute;
    right: 150px;
    height: 30px;
    font-size: 20px;
    top: 464px;
    bottom: 0;
    margin: auto;
    opacity: 0.25;
  }
   .upload-file:hover {
    opacity: 1;
  }
   .upload-file {
      right: 90px;
    }*/
  /*html,
  body,
  div,
  span {
    height: 100%;
    width: 100%;
    overflow: hidden;
    padding: 0;
    margin: 0;
    box-sizing: border-box;
  }
  */
  /*body {
    background: url("http://shurl.esy.es/y") no-repeat fixed center;
    background-size: cover;
  }*/

  .fa-2x {
    font-size: 1.5em;
  }

  .app {
    position: relative;
    overflow: hidden;
    top: 19px;
    height: calc(100% - 38px);
    margin: auto;
    padding: 0;
    box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .06), 0 2px 5px 0 rgba(0, 0, 0, .2);
  }

  .app-one {
    background-color: #f7f7f7;
    height: 523px;
    overflow: hidden;
    margin: 0;
    padding: 0;
    box-shadow: 0 1px 1px 0 rgba(0, 0, 0, .06), 0 2px 5px 0 rgba(0, 0, 0, .2);
  }

  .side {
    padding: 0;
    margin: 0;
    height: 100%;
  }
  .side-one {
    padding: 0;
    margin: 0;
    height: 100%;
    width: 100%;
    z-index: 1;
    position: relative;
    display: block;
    top: 0;
  }

  .side-two {
    padding: 0;
    margin: 0;
    height: 100%;
    width: 100%;
    z-index: 2;
    position: relative;
    top: -100%;
    left: -100%;
    -webkit-transition: left 0.3s ease;
    transition: left 0.3s ease;

  }


  .heading {
    padding: 10px 16px 10px 15px;
    margin: 0;
    height: 60px;
    width: 100%;
    background-color: #eee;
    z-index: 1000;
  }

  .heading-avatar {
    padding: 0;
    cursor: pointer;

  }

  .heading-avatar-icon img {
    border-radius: 50%;
    height: 40px;
    width: 40px;
  }

  .heading-name {
    padding: 0 !important;
    cursor: pointer;
  }

  .heading-name-meta {
    font-weight: 700;
    font-size: 100%;
    padding: 5px;
    padding-bottom: 0;
    text-align: left;
    text-overflow: ellipsis;
    white-space: nowrap;
    color: #000;
    display: block;
  }
  .heading-online {
    display: none;
    padding: 0 5px;
    font-size: 12px;
    color: #93918f;
  }
  .heading-compose {
    padding: 0;
  }

  .heading-compose i {
    text-align: center;
    padding: 5px;
    color: #93918f;
    cursor: pointer;
  }

  .heading-dot {
    padding: 0;
    margin-left: 10px;
  }

  .heading-dot i {
    text-align: right;
    padding: 5px;
    color: #93918f;
    cursor: pointer;
  }

  .searchBox {
    padding: 0 !important;
    margin: 0 !important;
    height: 60px;
    width: 100%;
  }

  .searchBox-inner {
    height: 100%;
    width: 100%;
    padding: 10px !important;
    background-color: #fbfbfb;
  }


  #searchBox-inner input {
    box-shadow: none;
  }

  .searchBox-inner input:focus {
    outline: none;
    border: none;
    box-shadow: none;
  }

  .sideBar {
    padding: 0 !important;
    margin: 0 !important;
    background-color: #fff;
    overflow-y: auto;
    border: 1px solid #f7f7f7;
    height: calc(100% - 120px);
  }

  .sideBar-body {
    position: relative;
    padding: 10px !important;
    border-bottom: 1px solid #f7f7f7;
    height: 72px;
    margin: 0 !important;
    cursor: pointer;
  }

  .sideBar-body:hover {
    background-color: #f2f2f2;
  }

  .sideBar-avatar {
    text-align: center;
    padding: 0 !important;
  }

  .avatar-icon img {
    border-radius: 50%;
    height: 49px;
    width: 49px;
  }

  .sideBar-main {
    padding: 0 !important;
  }

  .sideBar-main .row {
    padding: 0 !important;
    margin: 0 !important;
  }

  .sideBar-name {
    padding: 10px !important;
  }

  .name-meta {
    font-size: 100%;
    padding: 1% !important;
    text-align: left;
    text-overflow: ellipsis;
    white-space: nowrap;
    color: #000;
  }

  .sideBar-time {
    padding: 10px !important;
  }

  .time-meta {
    text-align: right;
    font-size: 12px;
    padding: 1% !important;
    color: rgba(0, 0, 0, .4);
    vertical-align: baseline;
  }

  /*New Message*/

  .newMessage {
    padding: 0 !important;
    margin: 0 !important;
    height: 100%;
    position: relative;
    left: -100%;
  }
  .newMessage-heading {
    padding: 10px 16px 10px 15px !important;
    margin: 0 !important;
    height: 100px;
    width: 100%;
    background-color: #00bfa5;
    z-index: 1001;
  }

  .newMessage-main {
    padding: 10px 16px 0 15px !important;
    margin: 0 !important;
    height: 60px;
    margin-top: 30px !important;
    width: 100%;
    z-index: 1001;
    color: #fff;
  }

  .newMessage-title {
    font-size: 18px;
    font-weight: 700;
    padding: 10px 5px !important;
  }
  .newMessage-back {
    text-align: center;
    vertical-align: baseline;
    padding: 12px 5px !important;
    display: block;
    cursor: pointer;
  }
  .newMessage-back i {
    margin: auto !important;
  }

  .composeBox {
    padding: 0 !important;
    margin: 0 !important;
    height: 60px;
    width: 100%;
  }

  .composeBox-inner {
    height: 100%;
    width: 100%;
    padding: 10px !important;
    background-color: #fbfbfb;
  }

  .composeBox-inner input:focus {
    outline: none;
    border: none;
    box-shadow: none;
  }

  .compose-sideBar {
    padding: 0 !important;
    margin: 0 !important;
    background-color: #fff;
    overflow-y: auto;
    border: 1px solid #f7f7f7;
    height: calc(100% - 160px);
  }

  /*Conversation*/

  .conversation {
    padding: 0 !important;
    margin: 0 !important;
    height: 100%;
    /*width: 100%;*/
    border-left: 1px solid rgba(0, 0, 0, .08);
    /*overflow-y: auto;*/
  }

  .message {
    padding: 0 !important;
    margin: 0 !important;
    background: url("w.jpg") no-repeat fixed center;
    background-size: cover;
    overflow-y: auto;
    border: 1px solid #f7f7f7;
    height: calc(100% - 120px);
  }
  .message-previous {
    margin : 0 !important;
    padding: 0 !important;
    height: auto;
    width: 100%;
  }
  .previous {
    font-size: 15px;
    text-align: center;
    padding: 10px !important;
    cursor: pointer;
  }

  .previous a {
    text-decoration: none;
    font-weight: 700;
  }

  .message-body {
    margin: 0 !important;
    padding: 0 !important;
    width: auto;
    height: auto;
  }

  .message-main-receiver {
    padding: 3px 20px !important;
    max-width: 60%;
  }

  .message-main-sender {
    padding: 3px 20px !important;
    margin-left: 40% !important;
    max-width: 60%;
  }

  .message-text {
    margin: 0 !important;
    padding: 5px !important;
    word-wrap:break-word;
    font-weight: 200;
    font-size: 14px;
    padding-bottom: 0 !important;
  }

  .message-time {
    margin: 0 !important;
    margin-left: 50px !important;
    font-size: 12px;
    text-align: right;
    color: #9a9a9a;

  }

  .receiver {
    width: auto !important;
    padding: 4px 10px 7px !important;
    border-radius: 10px 10px 10px 0;
    background: #ffffff;
    font-size: 12px;
    text-shadow: 0 1px 1px rgba(0, 0, 0, .2);
    word-wrap: break-word;
    display: inline-block;
    line-height: 10px;
  }

  .sender {
    float: right;
    width: auto !important;
    background: #dcf8c6;
    border-radius: 10px 10px 0 10px;
    padding: 4px 10px 7px !important;
    font-size: 12px;
    text-shadow: 0 1px 1px rgba(0, 0, 0, .2);
    display: inline-block;
    word-wrap: break-word;
    line-height: 10px;
  }


  /*Reply*/

  .reply {
    height: 60px;
    width: 100%;
    background-color: #f5f1ee;
    padding: 10px 5px 10px 5px !important;
    margin: 0 !important;
    z-index: 1000;

    display: inline-flex;
  }

  .reply-emojis {
    padding: 5px !important;
  }

  .reply-emojis i {
    text-align: center;
    padding: 5px 5px 5px 5px !important;
    color: #93918f;
    cursor: pointer;
  }

  .reply-recording {
    padding: 5px !important;
  }

  .reply-recording i {
    text-align: center;
    padding: 5px !important;
    color: #93918f;
    cursor: pointer;
  }

  .reply-send {
    padding: 5px !important;
  }

  .reply-send i {
    text-align: center;
    padding: 5px !important;
    color: #93918f;
    cursor: pointer;
  }

  .reply-main {
    padding: 2px 5px !important;
  }

  .reply-main textarea {
    width: 175%;
    resize: none;
    overflow: hidden;
    padding: 5px !important;
    outline: none;
    border: none;
    text-indent: 5px;
    box-shadow: none;
    height: 100%;
    font-size: 16px;
    margin-left: 65px;
  }

  .reply-main textarea:focus {
    outline: none;
    border: none;
    text-indent: 5px;
    box-shadow: none;
  }

  @media screen and (max-width: 700px) {
    .app {
      top: 0;
      height: 100%;
    }
    .heading {
      height: 70px;
      background-color: #009688;
    }
    .fa-2x {
      font-size: 2.3em !important;
    }
    .heading-avatar {
      padding: 0 !important;
    }
    .heading-avatar-icon img {
      height: 50px;
      width: 50px;
    }
    .heading-compose {
      padding: 5px !important;
    }
    .heading-compose i {
      color: #fff;
      cursor: pointer;
    }
    .heading-dot {
      padding: 5px !important;
      margin-left: 10px !important;
    }
    .heading-dot i {
      color: #fff;
      cursor: pointer;
    }
    .sideBar {
      height: calc(100% - 130px);
    }
    .sideBar-body {
      height: 80px;
    }
    .sideBar-avatar {
      text-align: left;
      padding: 0 8px !important;
    }
    .avatar-icon img {
      height: 55px;
      width: 55px;
    }
    .sideBar-main {
      padding: 0 !important;
    }
    .sideBar-main .row {
      padding: 0 !important;
      margin: 0 !important;
    }
    .sideBar-name {
      padding: 10px 5px !important;
    }
    .name-meta {
      font-size: 16px;
      padding: 5% !important;
    }
    .sideBar-time {
      padding: 10px !important;
    }
    .time-meta {
      text-align: right;
      font-size: 14px;
      padding: 4% !important;
      color: rgba(0, 0, 0, .4);
      vertical-align: baseline;
    }
    /*Conversation*/
    .conversation {
      padding: 0 !important;
      margin: 0 !important;
      height: 100%;
      /*width: 100%;*/
      border-left: 1px solid rgba(0, 0, 0, .08);
      /*overflow-y: auto;*/
    }
    .message {
      height: calc(100% - 140px);
    }
    .reply {
      height: 70px;
    }
    .reply-emojis {
      padding: 5px 0 !important;
    }
    .reply-emojis i {
      padding: 5px 2px !important;
      font-size: 1.8em !important;
    }
    .reply-main {
      padding: 2px 8px !important;
    }
    .reply-main textarea {
      padding: 8px !important;
      font-size: 18px;
    }
    .reply-recording {
      padding: 5px 0 !important;
    }
    .reply-recording i {
      padding: 5px 0 !important;
      font-size: 1.8em !important;
    }
    .reply-send {
      padding: 5px 0 !important;
    }
    .reply-send i {
      padding: 5px 2px 5px 0 !important;
      font-size: 1.8em !important;
    }
  }
  .side-bar{
    background: lightgray;
  }
</style>

</div>
</div>
@endsection
@section('scripts')
<script>
  $(document).ready(function(){
    $("#searchText").on("keyup", function() {
      var value = $(this).val().toLowerCase();
      $(".driver_user").filter(function() {
        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
      });
    });
  });

  
  var thread_id ='';
  function validate(){
    var status=null;
    var message = document.getElementById('message').value;
    if (message == '') {
        $('.inactive_doc').show();
        $('.inactive_msg').text('Please write a message!');
        status = false
    } else {
        $('.inactive_doc').hide();
        $('.inactive_msg').text('');
        status=true;
    }
    return status
  }

    $(document).on('submit','#messageForm', function(event){
        event.preventDefault();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        var t = new Date().toString().split(" ")[5];
        var a = t.split('');
        a.splice(6,0,':');
        var timeZone = a.join('');
        // var thread_id = $('#thread_id').val();
        var message = $('#message').val();
        var user_scope = 'admin-service';
        $.ajax({
            url: "{{ route('admin.chat.message.support') }}",
            method:"POST",
            data:{ thread_id: thread_id, user_scope: user_scope, message: message, timeZone: timeZone },
            success: function(response){
                console.log(response);
                //getMessage(thread_id)
                $('#message').val('');
            },
            error: function(response){
              if (response.status==422) {
                if (response.responseJSON.field=='thread_id') {
                  $('.inactive_doc').show();
                  $('.inactive_msg').text('Please select a driver!');
                }
                if (response.responseJSON.field=='message') {
                  $('.inactive_doc').show();
                  $('.inactive_msg').text('Please write a message!');
                }
              }
            }
        });
    });

  timeAgo = (date) => {
    var ms = (new Date()).getTime() - date.getTime();
    var seconds = Math.floor(ms / 1000);
    var minutes = Math.floor(seconds / 60);
    var hours = Math.floor(minutes / 60);
    var days = Math.floor(hours / 24);
    var months = Math.floor(days / 30);
    var years = Math.floor(months / 12);
    if (ms === 0) {
        return 'just now';
    } if (seconds < 0) {
        return '0 second ago';
    } if (seconds === 0 || seconds === 1) {
        return seconds + ' second ago';
    } if (seconds < 60 && seconds > 1) {
        return seconds + ' seconds ago';
    } if (minutes === 1) {
        return minutes + ' minute ago';
    } if (minutes < 60 && minutes > 1) {
        return minutes + ' minutes ago';
    } if (hours === 1) {
        return hours + ' hour ago';
    } if (hours < 24 && hours > 1) {
        return hours + ' hours ago';
    } if (days === 1) {
        return 'yesterday';
    } else {
        return moment(date).format('YYYY-MMM-DD');
    }
  }

  function getMessage(){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
    var t = new Date().toString().split(" ")[5];
    var a = t.split('');
    a.splice(6,0,':');
    var timeZone = a.join('');
    thread_id=thread_id;
    var user_scope = 'admin-service';
    $.ajax({
            url: "{{ route('admin.chat.message.support.get') }}",
            method:"POST",
            data:{ thread_id: thread_id, user_scope: user_scope, timeZone: timeZone },
            success: function(response){
                $('#conversation').html('');
                var options='';
                $.each(response.data, function(key, value) {
                    console.log(value.user_scope);

                    if (value.user_scope=='driver-service') {

                        options+=`
                          <div class="row message-body">
                            <div class="col-sm-12 message-main-receiver">
                              <div class="receiver">
                                <div class="message-text">
                                  <p class="text-muted pt-0">`+value.fname+` `+value.lname+`</p>
                                  <p class="py-0" style="color: black;line-height: initial;">`+value.message+`</p>
                                </div>
                                <div style="text-align: right;">
                                  <span class="text-right text-muted pb-0" style="font-size: 9px;">`+timeAgo(new Date(value.updated_at))+`</span>
                                </div>
                              </div>
                            </div>
                          </div>`;
                    }

                    if (value.user_scope=='admin-service') {

                        options+=`
                          <div class="row message-body">
                            <div class="col-sm-12 message-main-sender">
                              <div class="sender">
                                <div class="message-text">
                                  <p class="text-muted pt-0" style="text-align: right;">`+value.fname+` `+value.lname+`</p>
                                  <p class="py-0" style="color: black;line-height: initial;">`+value.message+`</p>
                                </div>
                                <span class="text-left text-muted pb-0" style="font-size: 9px;">`+timeAgo(new Date(value.updated_at))+`</span>
                              </div>
                            </div>
                          </div>`;
                    }

                });
                $('#conversation').prepend(options);
                setTimeout(function(){
                  getMessage()
                }, 5000);
            },
            error: function(response){
            }
        });
  }

  $('.driver_user').on('click', function(){
    $('.inactive_doc').hide();
    var isClass = $('.driver_user').hasClass('side-bar');
    if (isClass) {
      $('.driver_user').removeClass('side-bar');
    }
    $(this).addClass('side-bar');

    var user_id = $(this).attr('id');
    $.ajax({
      url: "{{route('admin.driver.get.byid')}}?user_id="+user_id,
      method: "GET",
      success: function(response){
        $('#driver_name').text(response.data.first_name+' '+response.data.last_name)
        var picture = response.data.picture;
        if(picture!=null){
          $("#driver_img").attr({ "src": picture });
          //$("#driver_img").css({ "width":"80px","height":"80px","border":"4px solid #38a3b8","border-radius":"50%","object-fit":"cover" });
        }else{
          $("#driver_img").attr({ "src": "{{URL::asset('/')}}asset/img/profile.png" });
        }
        $('#thread_id').val(user_id);
        thread_id = response.data.user_id;
        getMessage()
      }
    })
  })

</script>
@endsection
