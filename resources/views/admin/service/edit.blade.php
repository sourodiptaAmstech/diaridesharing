@extends('admin.layout.base')

@section('title', 'Update Service Type ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.service.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Service Type</h5>

            <form class="form-horizontal" action="{{route('admin.service.update', $service->id )}}" method="POST" enctype="multipart/form-data" role="form">
                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">

                <div class="form-group row">
                    <label for="name" class="col-xs-2 col-form-label">Service Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->name }}" name="name" required id="name" placeholder="Service Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="name_pt" class="col-xs-2 col-form-label">Service Name(Portuguese)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->name_pt }}" name="name_pt" required id="name_pt" placeholder="Service Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="provider_name" class="col-xs-2 col-form-label">Provider Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->provider_name }}" name="provider_name" id="provider_name" placeholder="Provider Name" required>
                    </div>
                </div>

                <div class="form-group row">
                    
                    <label for="image" class="col-xs-2 col-form-label">Picture</label>
                    <div class="col-xs-10">
                        @if(isset($service->image))
                            @if(File::exists(storage_path('app/public' .str_replace("storage", "", $service->image))))
                                <img style="height: 90px; margin-bottom: 15px;" src="{{URL::asset($service->image)}}">
                            @else
                                <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 60px; margin-bottom: 15px;">
                            @endif
                        @else
                        <img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                        @endif
                        <input type="file" accept="image/*" name="image" class="dropify form-control-file" id="image" aria-describedby="fileHelp">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fixed" class="col-xs-2 col-form-label">Base Price (fixed)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->fixed }}" name="fixed" required id="fixed" placeholder="Base Price">
                    </div>
                </div>

                {{-- <div class="form-group row">
                    <label for="distance" class="col-xs-2 col-form-label">Base Distance (every 5km)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->distance }}" name="distance" required id="distance" placeholder="Base Distance">
                    </div>
                </div> --}}

                <div class="form-group row">
                    <label for="minute" class="col-xs-2 col-form-label">Fare Per Minute</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->minute }}" name="minute" required id="minute" placeholder="Unit Time Pricing">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-xs-2 col-form-label">Fare Per Distance (km)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->price }}" name="price" required id="price" placeholder="Unit Distance Price">
                    </div>
                </div>

                 <div class="form-group row">
                    <label for="capacity" class="col-xs-2 col-form-label">Seat Capacity</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" value="{{ $service->capacity }}" name="capacity" required id="capacity" placeholder="Seat Capacity">
                    </div>
                </div>

                {{-- <div class="form-group row">
                    <label for="calculator" class="col-xs-2 col-form-label">Pricing Logic</label>
                    <div class="col-xs-10">
                        <select class="form-control" id="calculator" name="calculator">
                            <option value="MIN" @if($service->calculator =='MIN') selected @endif>@lang('servicetypes.MIN')</option>
                            <option value="HOUR" @if($service->calculator =='HOUR') selected @endif>@lang('servicetypes.HOUR')</option>
                            <option value="DISTANCE" @if($service->calculator =='DISTANCE') selected @endif>@lang('servicetypes.DISTANCE')</option>
                            <option value="DISTANCEMIN" @if($service->calculator =='DISTANCEMIN') selected @endif>@lang('servicetypes.DISTANCEMIN')</option>
                            <option value="DISTANCEHOUR" @if($service->calculator =='DISTANCEHOUR') selected @endif>@lang('servicetypes.DISTANCEHOUR')</option>
                        </select>
                    </div>
                </div> --}}


                <div class="form-group row">
                    <label for="description" class="col-xs-2 col-form-label">Description</label>
                    <div class="col-xs-10">
                        <textarea class="form-control" type="number" value="{{ $service->description }}" name="description" id="description" required placeholder="Description" rows="4">{{ $service->description }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="insure_price" class="col-xs-2 col-form-label">Safe Ride Fare(insure price)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->insure_price }}" name="insure_price" required id="insure_price" placeholder="Safe Ride Fee">
                    </div>
                </div>
                
                {{-- <div class="form-group row">
                    <label for="min_price" class="col-xs-2 col-form-label">Minimum Fare</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->min_price }}" name="min_price" required id="min_price" placeholder="Minimum Fare">
                    </div>
                </div> --}}

                <div class="form-group row">
                    <label for="min_waiting_time" class="col-xs-2 col-form-label">Minimum Waiting Time</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->min_waiting_time }}" name="min_waiting_time" required id="min_waiting_time" placeholder="Minimum Waiting Time">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="min_waiting_charge" class="col-xs-2 col-form-label">Minimum Waiting Charge</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $service->min_waiting_charge }}" name="min_waiting_charge" required id="min_waiting_charge" placeholder="Minimum Waiting Charge">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="status" class="col-xs-2 col-form-label">Status</label>
                    <div class="col-xs-10">
                        <select class="form-control" id="status" name="status">
                            <option value="1" @if($service->status =='1') selected @endif>Active</option>
                            <option value="0" @if($service->status =='0'||$service->status =='-1') selected @endif>Inactive</option>
                        </select>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Update Service Type</button>
                        <a href="{{route('admin.service.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
@endsection