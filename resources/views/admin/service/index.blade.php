@extends('admin.layout.base')

@section('title', 'Service Types ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5 class="mb-1">Service Types</h5>
            <a href="{{ route('admin.service.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Service</a>
            <table class="table table-striped table-bordered dataTable" id="table-2">
                <thead>
                    <tr>
                        <th>S.No</th>
                        <th>Service Name</th>
                        <th>Provider Name</th>
                        <th>Capacity</th>
                        <th>Status</th>
                       {{--  <th>Safe Ride Fee</th>
                        <th>Base Price</th>
                        <th>Base Distance</th>
                        <th>Distance Price</th>
                        <th>Time Price</th>
                        <th class="none">Price Calculation</th> --}}
                        <th>Service Image</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                @foreach($services as $index => $service)
                    <tr>
                        <td>{{ $index + 1 }}</td>
                        <td>{{ $service->name }}<br>({{$service->name_pt}})</td>
                        <td>{{ $service->provider_name }}</td>
                        <td>{{ $service->capacity }}</td>
                        <td>
                            @if($service->status == 1) 
                                <p style="color:green;">Active</p>
                            @else 
                                <p style="color:red;">Inactive</p>
                            @endif
                        </td>
                      {{--   <td>{{ ($service->insure_price) }}</td>
                        <td>{{ ($service->fixed) }}</td>
                        <td>{{ ($service->distance) }}</td>
                        <td>{{ ($service->price) }}</td>
                        <td>{{ ($service->minute) }}</td>
                        <td>@lang('servicetypes.'.$service->calculator)</td> --}}
                       
                        <td>
                            @if(isset($service->image))
                                @if(File::exists(storage_path('app/public' .str_replace("storage", "", $service->image))))
                                <img src="{{URL::asset($service->image)}}" style="height: 50px; width: 65px;">
                                @else
                                <img src="{{URL::asset($service->image)}}" style="height: 50px; width: 65px;">
                                @endif
                            @else
                                <img src="{{URL::asset('asset/NO_IMG.png')}}" style="height: 50px; width: 65px;">
                            @endif
                        </td>
                        <td style="line-height: 34px;">
                            <a href="{{ route('admin.service.edit', $service->id) }}" class="btn btn-info"><i class="fa fa-pencil"></i>Edit</a>
                            <form action="{{ route('admin.service.destroy', $service->id) }}" class="service-delete" method="POST">
                                {{ csrf_field() }}
                                {{ method_field('DELETE') }}
                                <button class="btn btn-danger">
                                    <i class="fa fa-trash"></i> Delete
                                </button>
                            </form>
                        </td>
                    </tr>
                @endforeach
                </tbody>
                <tfoot>
                    <tr>
                        <th>S.No</th>
                        <th>Service Name</th>
                        <th>Provider Name</th>
                        <th>Capacity</th>
                        <th>Status</th>
                      {{--   <th>Safe Ride Fee</th>
                        <th>Base Price</th>
                        <th>Base Distance</th>
                        <th>Distance Price</th>
                        <th>Time Price</th>
                        <th class="none">Price Calculation</th> --}}
                        <th>Service Image</th>
                        <th>Action</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(document).on('click','.service-delete',function (e) {
            e.preventDefault();
            var form=$(this);
            bootbox.confirm('Do you really want to delete?', function (res) {
            if (res){
               form.submit();
            }
            });
        });
    });
</script>
@endsection