@extends('admin.layout.base')

@section('title', 'Add Service Type ')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.service.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Add Service Type</h5>

            <form class="form-horizontal" action="{{route('admin.service.store')}}" method="POST" enctype="multipart/form-data" role="form">
                {{ csrf_field() }}

                <div class="form-group row">
                    <label for="name" class="col-xs-2 col-form-label">Service Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('name') }}" name="name" required id="name" placeholder="Service Name">
                    </div>
                </div>
                <div class="form-group row">
                    <label for="name_pt" class="col-xs-2 col-form-label">Service Name(Portuguese)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('name_pt') }}" name="name_pt" required id="name_pt" placeholder="Service Name">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="provider_name" class="col-xs-2 col-form-label">Provider Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('provider_name') }}" name="provider_name" id="provider_name" placeholder="Provider Name" required>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="picture" class="col-xs-2 col-form-label">Service Image</label>
                    <div class="col-xs-10">
                        <input type="file" accept="image/*" name="image" class="dropify form-control-file" id="picture" aria-describedby="fileHelp">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="fixed" class="col-xs-2 col-form-label">Base Price (fixed)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('fixed') }}" name="fixed" required id="fixed" placeholder="Base Price">
                    </div>
                </div>

                {{-- <div class="form-group row">
                    <label for="distance" class="col-xs-2 col-form-label">Base Distance (every 5km)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('distance') }}" name="distance" required id="distance" placeholder="Base Distance">
                    </div>
                </div> --}}

                <div class="form-group row">
                    <label for="minute" class="col-xs-2 col-form-label">Fare Per Minute</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('minute') }}" name="minute" required id="minute" placeholder="Unit Time Pricing">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="price" class="col-xs-2 col-form-label">Fare Per Distance (km)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('price') }}" name="price" required id="price" placeholder="Unit Distance Price">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="capacity" class="col-xs-2 col-form-label">Seat Capacity</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" value="{{ old('capacity') }}" name="capacity" required id="capacity" placeholder="Capacity">
                    </div>
                </div>

                {{-- <div class="form-group row">
                    <label for="calculator" class="col-xs-2 col-form-label">Pricing Logic</label>
                    <div class="col-xs-10">
                        <select class="form-control" id="calculator" name="calculator">
                            <option value="MIN">@lang('servicetypes.MIN')</option>
                            <option value="HOUR">@lang('servicetypes.HOUR')</option>
                            <option value="DISTANCE">@lang('servicetypes.DISTANCE')</option>
                            <option value="DISTANCEMIN">@lang('servicetypes.DISTANCEMIN')</option>
                            <option value="DISTANCEHOUR">@lang('servicetypes.DISTANCEHOUR')</option>
                        </select>
                    </div>
                </div> --}}

                <div class="form-group row">
                    <label for="description" class="col-xs-2 col-form-label">Description</label>
                    <div class="col-xs-10">
                        <textarea class="form-control" type="number" value="{{ old('description') }}" name="description" id="description" required placeholder="Description" rows="4">{{ old('description') }}</textarea>
                    </div>
                </div>

                <div class="form-group row">
                    <label for="insure_price" class="col-xs-2 col-form-label">Safe Ride Fare(insure price)</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('insure_price') }}" name="insure_price" required id="insure_price" placeholder="Safe Ride Fee">
                    </div>
                </div>
                
                {{-- <div class="form-group row">
                    <label for="min_price" class="col-xs-2 col-form-label">Minimum Fare</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('min_price') }}" name="min_price" required id="min_price" placeholder="Minimum Fare">
                    </div>
                </div> --}}

                <div class="form-group row">
                    <label for="min_waiting_time" class="col-xs-2 col-form-label">Minimum Waiting Time</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('min_waiting_time') }}" name="min_waiting_time" required id="min_waiting_time" placeholder="Minimum Waiting Time">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="min_waiting_charge" class="col-xs-2 col-form-label">Minimum Waiting Charge</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ old('min_waiting_charge') }}" name="min_waiting_charge" required id="min_waiting_charge" placeholder="Minimum Waiting Charge">
                    </div>
                </div>

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Add Service Type</button>
                        <a href="{{route('admin.service.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
                
            </form>
        </div>
    </div>
</div>
@endsection
