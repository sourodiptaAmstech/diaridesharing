@extends('admin.layout.base')

@section('title', 'Promocodes ')

@section('content')

    <div class="content-area py-1">
        <div class="container-fluid">
            
            <div class="box box-block bg-white">
                <h5 class="mb-1">Promocodes</h5>
                <a href="{{ route('admin.promocode.create') }}" style="margin-left: 1em;" class="btn btn-primary pull-right"><i class="fa fa-plus"></i> Add New Promocode</a>

                <table class="table table-striped table-bordered dataTable" id="table-2">
                    <thead>
                        <tr>
                            <th>S.No</th>
                            <th>Promocode </th>
                            <th>Discount </th>
                            <th>Expiration</th>
                            <th>Status</th>
                            <th>Used Count</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($promocodes as $index => $promo)
                        <tr>
                            <td>{{$index + 1}}</td>
                            <td>{{$promo->promo_code}}</td>
                            <td>{{$promo->discount}}</td>
                            <td>
                                {{date('d-m-Y',strtotime($promo->expiration))}}
                            </td>
                            <td>
                                @if(date("Y-m-d") <= $promo->expiration)
                                    <span class="tag tag-success">Valid</span>
                                @else
                                    <span class="tag tag-danger">Expiration</span>
                                @endif
                            </td>
                            <td>
                               {{App\Http\Controllers\Admin\Promocode\PromocodeController::promo_used_count($promo->promocodes_id)}}
                            </td>
                            <td style="line-height: 34px;">
                                <a href="{{ route('admin.promocode.edit', $promo->promocodes_id) }}" class="btn btn-info"> Edit</a>
                                <form action="{{ route('admin.promocode.destroy', $promo->promocodes_id) }}" method="POST" style="line-height: 30px;" class="promo-delete">
                                    {{ csrf_field() }}
                                    <input type="hidden" name="_method" value="DELETE">
                                    
                                @if(date("Y-m-d") <= $promo->expiration)
                                @else
                                    @if(App\Http\Controllers\Admin\Promocode\PromocodeController::promo_used_count($promo->promocodes_id)==0)
                                        <button class="btn btn-danger"><i class="fa fa-trash"></i> Delete</button>
                                    @endif
                                @endif 
                                </form>
                                {{-- <a href="{{ route('admin.promocode.show', $promo->promocodes_id) }}" class="btn btn-info"> Show</a> --}}
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th>S.No</th>
                            <th>Promocode </th>
                            <th>Discount </th>
                            <th>Expiration</th>
                            <th>Status</th>
                            <th>Used Count</th>
                            <th>Action</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
            
        </div>
    </div>
@endsection
@section('scripts')
<script>
    $(document).ready(function(){
        $(document).on('click','.promo-delete',function (e) {
            e.preventDefault();
            var form=$(this);
            bootbox.confirm('Do you really want to delete?', function (res) {
            if (res){
               form.submit();
            }
            });
        });
    });
</script>
@endsection