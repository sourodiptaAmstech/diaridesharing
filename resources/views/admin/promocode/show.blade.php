@extends('admin.layout.base')

@section('title', 'Promocode Used By')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
    	<div class="box box-block bg-white">
            <a href="{{ route('admin.promocode.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Promocode Used By</h5>
            
            <div class="row">
                <div class="col-md-6">
                    @foreach($PromocodeUsage as $promocode)
                    <dl class="row">
                        <dt class="col-sm-6">
                            @if($promocode->passenger->picture=='')
                            <img src="{{URL::asset('asset/img/profile.png')}}" class="img-rounded1" style="width: 40%;height: auto;object-fit: cover;border-radius: 50%;">
                            @else
                            <img src="{{$promocode->passenger->picture}}" class="img-rounded1" style="width: 40%;height: auto;object-fit: cover;border-radius: 50%;">
                            @endif
                        </dt>
                        <dd class="col-sm-6">{{$promocode->passenger->first_name}} {{$promocode->passenger->last_name}}</dd>
                    </dl>
                    @endforeach
                </div>
            </div>

            {{-- <form class="form-horizontal" action="{{route('admin.promocode.store')}}" method="POST" enctype="multipart/form-data" role="form">
            	{{csrf_field()}}
				<div class="form-group row">
					<label for="promo_code" class="col-xs-2 col-form-label">Promocode</label>
					<div class="col-xs-10">
						<input class="form-control" autocomplete="off"  type="text" value="{{ old('promo_code') }}" name="promo_code" required id="promo_code" placeholder="Promocode">
					</div>
				</div>
				<div class="form-group row">
					<label for="discount" class="col-xs-2 col-form-label">Discount</label>
					<div class="col-xs-10">
						<input class="form-control" type="number" value="{{ old('discount') }}" name="discount" required id="discount" placeholder="Discount">
					</div>
				</div>

				<div class="form-group row">
					<label for="expiration" class="col-xs-2 col-form-label">Expiration</label>
					<div class="col-xs-10">
						<input class="form-control" type="date" value="{{ old('expiration') }}" name="expiration" required id="expiration" placeholder="Expiration">
					</div>
				</div>


				<div class="form-group row">
					<label for="zipcode" class="col-xs-2 col-form-label"></label>
					<div class="col-xs-10">
						<button type="submit" class="btn btn-primary">Add Promocode</button>
						<a href="{{route('admin.promocode.index')}}" class="btn btn-default">Cancel</a>
					</div>
				</div>
			</form> --}}
		</div>
    </div>
</div>

@endsection
