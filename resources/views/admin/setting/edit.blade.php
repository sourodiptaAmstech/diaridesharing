@extends('admin.layout.base')

@section('title', 'Update Site Setting')

@section('content')

<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <a href="{{ route('admin.setting.index') }}" class="btn btn-default pull-right"><i class="fa fa-angle-left"></i> Back</a>

            <h5 style="margin-bottom: 2em;">Update Site Setting</h5>

            <form class="form-horizontal" action="{{route('admin.setting.update', $site_setting->id )}}" method="POST" enctype="multipart/form-data" role="form">

                {{csrf_field()}}
                <input type="hidden" name="_method" value="PATCH">
                <div class="form-group row">
                    <label for="key" class="col-xs-2 col-form-label">Name</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $site_setting->key }}" name="key" id="key" placeholder="Key" readonly>
                    </div>
                </div>
                @if($site_setting->key == 'site_title'||$site_setting->key == 'site_copyright'||$site_setting->key == 'web_site')
                <div class="form-group row">
                    <label for="value" class="col-xs-2 col-form-label">Value</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="text" value="{{ $site_setting->value }}" name="value"  id="value" placeholder="Value" required>
                    </div>
                </div>
                @elseif($site_setting->key == 'contact_email')
                <div class="form-group row">
                    <label for="value" class="col-xs-2 col-form-label">Value</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="email" value="{{ $site_setting->value }}" name="value"  id="value" placeholder="Value" required>
                    </div>
                </div>

                @elseif($site_setting->key == 'sos_email')
                <div class="form-group row">
                    <label for="value" class="col-xs-2 col-form-label">SOS Emails</label>
                    <div class="col-xs-10">
                        @if(isset($site_setting->value))
                            <select name="value[]" id="select1" class="form-control" multiple>
                            @foreach(json_decode($site_setting->value) as $key => $to_em)
                                 <option value="{{$to_em}}"></option>
                            @endforeach
                            </select>
                        @else
                            <select name="value[]" class="form-control" multiple></select>
                        @endif
                    </div>
                </div>

                @elseif($site_setting->key == 'contact_number'||$site_setting->key == 'sos_number'||$site_setting->key == 'commission'||$site_setting->key == 'passenger_cancellation_charge'||$site_setting->key == 'driver_cancellation_charge')
                <div class="form-group row">
                    <label for="value" class="col-xs-2 col-form-label">Value</label>
                    <div class="col-xs-10">
                        <input class="form-control" type="number" value="{{ $site_setting->value }}" name="value"  id="value" placeholder="Value" required>
                    </div>
                </div>

                @elseif($site_setting->key == 'site_logo'||$site_setting->key == 'site_email_logo'||$site_setting->key == 'site_icon')
                <div class="form-group row">
                    <label for="value" class="col-xs-2 col-form-label">Logo</label>
                    <div class="col-xs-10">
                    @if(isset($site_setting->value))
                        @if(File::exists(storage_path('app/public' .str_replace("storage", "", $site_setting->value))))
                        <img style="height: 90px; margin-bottom: 15px;" src="{{URL::asset($site_setting->value)}}">
                        @else
                        <img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                        @endif
                    @else
                        <img style="height: 60px; margin-bottom: 15px;" src="{{URL::asset('asset/NO_IMG.png')}}">
                    @endif
                        <input type="file" accept="image/*" name="value" class="dropify form-control-file" id="value" aria-describedby="fileHelp">
                    </div>
                </div>
                @endif

                <div class="form-group row">
                    <label for="zipcode" class="col-xs-2 col-form-label"></label>
                    <div class="col-xs-10">
                        <button type="submit" class="btn btn-primary">Update Site Setting</button>
                        <a href="{{route('admin.setting.index')}}" class="btn btn-default">Cancel</a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<style type="text/css">
    .bootstrap-tagsinput input { width:100%!important; }
</style>
@endsection
@section('scripts')
<script src="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.min.js"></script>
<link rel="stylesheet" href="https://cdn.jsdelivr.net/bootstrap.tagsinput/0.4.2/bootstrap-tagsinput.css" />
<script>
    $("select").tagsinput();
    $('.bootstrap-tagsinput > input').prop( "style", null );
</script>
@endsection