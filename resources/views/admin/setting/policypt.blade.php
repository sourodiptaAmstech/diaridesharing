@extends('admin.layout.base')

@section('title', 'Privacy Policy')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Privacy Policy (Portuguese)</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'page_privacy_pt')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update.portuguese',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor">{{$policy->value}}</textarea>
                        </div>
                    </div>

                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            <h5>Terms and Conditions (Portuguese)</h5>

            @foreach($policies as $policy)
            @if($policy->key == 'condition_privacy_pt')
            <div className="row">
                <form action="{{ route('admin.privacy-policy.update.portuguese',$policy->id) }}" method="POST">
                    {{ csrf_field() }}
                    <input type="hidden" name="_method" value="PUT">

                    <div class="row">
                        <div class="col-xs-12">
                            <textarea name="value" id="myeditor1">{{$policy->value}}</textarea>
                        </div>
                    </div>
                    <br>
                    <div class="row">
                        <div class="col-xs-10">
                            <button type="submit" class="btn btn-primary">Update</button>
                            <a href="{{route('admin.dashboard.index')}}" class="btn btn-default">Cancel</a>
                        </div>
                    </div>
                </form>
            </div>
            @endif
            @endforeach

        </div>
    </div>
</div>
@endsection

@section('scripts')
<script src="//cdn.ckeditor.com/4.6.2/standard/ckeditor.js"></script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor');
</script>
<script type="text/javascript">
    CKEDITOR.replace('myeditor1');
</script>
@endsection