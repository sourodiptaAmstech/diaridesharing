@extends('admin.layout.auth')

<!-- Main Content -->
@section('content')
<div class="sign-form">
    <div class="row">
        <div class="col-md-4 offset-md-4 px-3">
            <div class="box b-a-0">
                <div class="p-2 text-xs-center pass">
                    <h5>Forgot Password</h5>
                </div>
                <div class="p-2 text-xs-center pass">
                    <h4>Enter Email ID</h4>
                    <p>We will send you an OTP to reset your password</p>
                </div>

                <p class="success text-success text-xs-center font-weight-bold" style="display: none; padding-top: 50px;">OTP has been sent to your Email ID, Please check your Email!</p>

                <form class="form-material mb-1" role="form">
                {{ csrf_field() }}
                    <div class="form-group {{ $errors->has('email') ? ' has-error' : '' }}">
                        <input type="email" name="email" value="{{ old('email') }}" required="true" class="form-control" id="email" placeholder="Email">
                        <p class="text-danger mesg-error text-xs-center"></p>
                        @if ($errors->has('email'))
                            <span class="help-block">
                                <strong>{{ $errors->first('email') }}</strong>
                            </span>
                        @endif
                    </div>

                    <div class="px-2 form-group mb-0">
                        <button type="submit" id="ajaxSubmit" class="btn btn-purple btn-block text-uppercase">
                            <span class="msg">Send Password Reset OTP</span>
                            <i class="spin fa fa-circle-o-notch fa-spin" style="font-size: 22px; display: none;"></i>
                        </button>
                    </div>
                </form>
                    <div class="px-2 form-group mb-0 text-xs-center">
                    <a class="glyphicon glyphicon-ok text-success" style="font-size: 50px; display: none; padding-top: 25px;" id="right"></a>
                    </div>

                <div class="p-2 text-xs-center text-muted">
                    <a class="text-black login" href="{{ url('/login') }}"><span class="underline">Login Here!</span></a>
                </div>
            </div>
        </div>
    </div>
</div>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
@endsection

@section('scripts')
<script>
    $(document).ready(function(){
       
        $('#ajaxSubmit').click(function(e){
            e.preventDefault();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
            var email = $('#email').val();
            $('.msg').css({'display':'none'});
            $('.spin').css({'display':'block'});
            $.ajax({
                url: "{{ route('password.email') }}",
                method: 'post',
                data: { email: email },
                success: function(response){
                    if (response.success) {
                        $('#ajaxSubmit').css({'display':'none'});
                        $('#email').css({'display':'none'});
                        $('.pass').css({'display':'none'});
                        $('.login').css({'display':'none'});
                        $('.mesg-error').css({'display':'none'});
                        $('.success').css({'display':'block'});
                        $('#right').css({'display':'block'}).delay(1000).fadeOut(1000, function(){
                            window.location.href = '{{ route("password.otp") }}';
                        });
                    }
                },

                error: function(response){
                    $('.msg').css({'display':'block'});
                    $('.spin').css({'display':'none'});
                    if (response.status == 422){
                        $('.mesg-error').show(0).delay(6000).hide(400);
                        setTimeout(function() { 
                            $('.mesg-error').fadeOut(); 
                        }, 6000);
                        $('.mesg-error').html(response.responseJSON.errors.email[0]);
                        $('.mesg-error').html(response.responseJSON.errors.email[1]);
                    }
                    if (response.status == 404){
                        $('.mesg-error').show(0).delay(6000).hide(400);
                        setTimeout(function() { 
                            $('.mesg-error').fadeOut(); 
                        }, 6000);
                        $('.mesg-error').html(response.responseJSON.message);
                    }
                    if (response.status == 500){
                        $('.mesg-error').show(0).delay(6000).hide(400);
                        setTimeout(function() { 
                            $('.mesg-error').fadeOut(); 
                        }, 6000);
                        $('.mesg-error').text('Internal server error');
                    }
                }
            });
        });
    });

</script>
@endsection
