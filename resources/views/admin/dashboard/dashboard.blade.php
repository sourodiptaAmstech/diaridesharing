@extends('admin.layout.base')

@section('title', 'Dashboard ')

@section('styles')
	<link rel="stylesheet" href="{{asset('main/vendor/jvectormap/jquery-jvectormap-2.0.3.css')}}">
@endsection

@section('content')

<div class="content-area py-1">
<div class="container-fluid">
	<div class="row row-md">
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right" style="z-index: 12;"><a href="{{ route('admin.request.all') }}"><span class="bg-danger"></span><i class="ti-rocket"></i></a></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Completed <br>Rides</h6>
					<h1 class="mb-1">{{$totalRide}}</h1>
				</div>
			</div>
		</div>
		{{-- <div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-success"></span><i class="ti-bar-chart"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Revenue</h6>
					<h1 class="mb-1">
						@if(isset($totalEarn))
							${{$totalEarn}}
						@else
							0
						@endif
					</h1>
					<i class="fa fa-caret-up text-success mr-0-5"></i><span>from {{$rides->count()}} Rides</span>
				</div>
			</div>
		</div> --}}

		{{-- <div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right"><span class="bg-primary"></span><i class="ti-view-grid"></i></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">No. of Service Types</h6>
					<h1 class="mb-1">{{$service}}</h1>
				</div>
			</div>
		</div> --}}
		
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right" style="z-index: 12;"><a href="{{ route('admin.request.rejected') }}"><span class="bg-warning"></span><i class="ti-archive"></i></a></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total Rejected Rides</h6>
					<h1 class="mb-1">
						@if(isset($totalCancelRide))
							{{$totalCancelRide}}
						@else
						0
						@endif
					</h1>
					{{-- <i class="fa fa-caret-down text-danger mr-0-5"></i><span>for  
						@if(isset($totalCancelRide)) 
							@if($totalCancelRide == 0) 
								0.00%
							@else 
								{{round($totalCancelRide*100/$totalRide,2)}}% 
							@endif
						@else
						0.00%
						@endif
					Rides</span> --}}
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right" style="z-index: 12;"><a href="{{ route('admin.passenger.cancel.report') }}"><span class="bg-primary"></span><i class="ti-view-grid"></i></a></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">User Cancelled Count</h6>
					<h1 class="mb-1">{{$cancel_bypassenger}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right" style="z-index: 12;"><a href="{{ route('admin.driver.cancel.report') }}"><span class="bg-danger"></span><i class="ti-bar-chart"></i></a></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Driver Cancelled Count</h6>
					<h1 class="mb-1">{{$cancel_bydriver}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right" style="z-index: 12;"><a href="{{ route('admin.driver.index') }}"><span class="bg-warning"></span><i class="ti-rocket"></i></a></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Registered Drivers</h6>
					<h1 class="mb-1">{{$totalDriver}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right" style="z-index: 12;"><a href="{{ route('admin.request.schedule') }}"><span class="bg-success"></span><i class="ti-bar-chart"></i></a></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">No. of Scheduled Rides</h6>
					<h1 class="mb-1">{{$totalScheduleRide}}</h1>
				</div>
			</div>
		</div>
		<div class="col-lg-3 col-md-6 col-xs-12">
			<div class="box box-block bg-white tile tile-1 mb-2">
				<div class="t-icon right" style="z-index: 12;"><a href="{{ route('admin.driver.index') }}"><span class="bg-warning"></span><i class="ti-rocket"></i></a></div>
				<div class="t-content">
					<h6 class="text-uppercase mb-1">Total No. of Active <br>Drivers</h6>
					<h1 class="mb-1">{{$totalActiveDriver}}</h1>
				</div>
			</div>
		</div>
	</div>

	<div class="row row-md mb-2">
		<div class="col-md-12">
				<div class="box bg-white">
					<div class="box-block clearfix">
						<h5 class="float-xs-left">Recent Rides</h5>
					</div>
					<div class="table-responsive">
						<table class="table mb-md-0">
							<tbody>
							@foreach(@$serviceRequests as $index => $serviceRequest)
								<tr>
									<th scope="row">{{$index + 1}}</th>
									<td>{{@$serviceRequest->passenger->first_name}} {{@$serviceRequest->passenger->last_name}}</td>
									<td>{{$serviceRequest->request_no}}</td>
									<td>
										<span class="text-muted">{{$serviceRequest->created_at->diffForHumans()}}</span>
									</td>
									<td>
										@if($serviceRequest->request_status=='COMPLETED')
											<span class="tag tag-success">{{$serviceRequest->request_status}}</span>
										@elseif($serviceRequest->request_status=='NOSERVICEFOUND')
											<span class="tag tag-danger">NO SERVICE FOUND</span>
										@else
											<span class="tag tag-warning">ON RIDE</span>
										@endif
									</td>
									<td>
										<a class="text-primary" href="{{route('admin.dashboard.details',[$serviceRequest->request_id,'all'])}}"><span class="underline">View Ride Details</span></a>
									</td>
								</tr>
							@endforeach
							</tbody>
						</table>
					</div>
				</div>
			</div>

		</div>

	</div>
</div>
@endsection
