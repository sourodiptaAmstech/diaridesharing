@extends('admin.layout.base')

@section('title', 'Trip Details')

@section('content')
<div class="content-area py-1">
    <div class="container-fluid">
        <div class="box box-block bg-white">
            @if($param=='all')
            <a href="{{ route('admin.passenger.trip.history',[$serviceRequest->passenger_id]) }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='nofound')
            <a href="{{ route('admin.request.nofound') }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            @if($param=='rejected')
            <a href="{{ route('admin.request.rejected') }}" class="btn btn-default pull-right">
                <i class="fa fa-angle-left"></i> Back
            </a>
            @endif
            <h4>Trip Details</h4>
            <div class="row">
                <div class="col-md-6">
                    <dl class="row">
                        <dt class="col-sm-6">Passenger Name :</dt>
                        <dd class="col-sm-6">{{ $serviceRequest->passenger->first_name }} {{ $serviceRequest->passenger->last_name }} </dd>

                        @if($param=='rejected')
                        <dt class="col-sm-6">Booking Number :</dt>
                        <dd class="col-sm-6">{{ $serviceRequest->service_request->request_no }}</dd>

                        <dt class="col-sm-6">Payment Method :</dt>
                        <dd class="col-sm-6">
                            {{ $serviceRequest->service_request->payment_method }}</dd>
                        @else
                        <dt class="col-sm-6">Booking Number :</dt>
                        <dd class="col-sm-6">{{ $serviceRequest->request_no }}</dd>

                        <dt class="col-sm-6">Payment Method :</dt>
                        <dd class="col-sm-6">
                            {{ $serviceRequest->payment_method }}</dd>
                        @endif

                        @if($param=='rejected')
                            <dt class="col-sm-6">Driver Action :</dt>
                            <dd class="col-sm-6">
                                DECLINE
                            </dd>
                        @else
                            <dt class="col-sm-6">Booking Status :</dt>
                            <dd class="col-sm-6">
                            @if($serviceRequest->request_status=='NOSERVICEFOUND')
                                NO SERVICE FOUND
                            @elseif($serviceRequest->request_status=='CANCELBYSYSTEM'||$serviceRequest->request_status=='CANCELBYPASSENGER'||$serviceRequest->request_status=='CANCELBYDRIVER')
                                CANCEL
                            @else
                                {{$serviceRequest->request_status}}
                            @endif
                            </dd>
                        @endif


                        {{-- @if($param=='all') --}}
                        @if($serviceRequest->request_status=='NOSERVICEFOUND')
                        @elseif($serviceRequest->request_status=='CANCELBYSYSTEM')
                        @elseif($serviceRequest->request_status=='CANCELBYPASSENGER')
                        @elseif($serviceRequest->request_status=='CANCELBYDRIVER')
                        @else
                        <dt class="col-sm-6">Driver Name :</dt>
                        <dd class="col-sm-6">{{ $serviceRequest->driver->first_name }} {{ $serviceRequest->driver->last_name }} </dd>
                        @endif
                        {{-- @endif --}}

                        @if($serviceRequest->request_status=='CANCELBYPASSENGER')
                        @else
                        <dt class="col-sm-6">Service Type :</dt>
                        <dd class="col-sm-6">{{ $serviceRequest->service_type->name }}</dd>
                        @endif
                        {{-- @if($param=='all') --}}
                        @if($serviceRequest->request_status=='NOSERVICEFOUND')
                        @elseif($serviceRequest->request_status=='CANCELBYSYSTEM')
                        @elseif($serviceRequest->request_status=='CANCELBYPASSENGER')
                        @elseif($serviceRequest->request_status=='CANCELBYDRIVER')
                        @else
                            @if(isset($DriverServiceType))
                            <dt class="col-sm-6">Car Model :</dt>
                            <dd class="col-sm-6">{{ $DriverServiceType->model }}</dd>
                            <dt class="col-sm-6">Car Number :</dt>
                            <dd class="col-sm-6">{{ $DriverServiceType->registration_no }}</dd>
                            @endif
                            @if($param=='rejected')
                            @else
                                @if(isset($serviceRequest->transaction))
                                <dt class="col-sm-6">Distance :</dt>
                                <dd class="col-sm-6">{{ $serviceRequest->transaction->distance_miles }} miles</dd>
                                <dt class="col-sm-6">Duration :</dt>
                                <dd class="col-sm-6">
                                    @if($serviceRequest->transaction->duration_hr!=0)
                                    {{ $serviceRequest->transaction->duration_hr }} hrs.
                                    @endif
                                    @if($serviceRequest->transaction->duration_min!=0)
                                    {{ $serviceRequest->transaction->duration_min }} min.
                                    @endif
                                </dd>
                                @endif
                            @endif
                        @endif
                        {{-- @endif --}}


                        <dt class="col-sm-6">PickUp Address :</dt>
                        <dd class="col-sm-6">{{ $serviceRequest->s_address }}</dd>
                        <dt class="col-sm-6">Drop Address :</dt>
                        <dd class="col-sm-6">{{ $serviceRequest->d_address }}</dd>

                        @if($serviceRequest->request_status=='NOSERVICEFOUND')
                        @if(count($request_logs)>0)
                        <dt class="col-sm-6 font-weight-bold text-info" style="font-size: 18px;">Request Log</dt><dd class="col-sm-6"></dd>
                        <table class="table table-bordered">
                            <thead style="background-color: lightgray;">
                                <tr>
                                    <th>Driver Name</th>
                                    <th>Service Type</th>
                                    <th>Car Number</th>
                                    <th>Model</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($request_logs as $request_log)
                                <tr>
                                   <td>
                                    {{$request_log->first_name}} {{$request_log->last_name}}
                                    </td>
                                   <td>{{$request_log->name}}</td>
                                   <td>{{$request_log->registration_no}}</td>
                                   <td>{{$request_log->model}}</td>
                                </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @endif
                        @endif

                        @if($param=='all')
                            @if($serviceRequest->request_status=='NOSERVICEFOUND')
                            @else
                            <dt class="col-sm-6">PickUp Time :</dt>
                            <dd class="col-sm-6">
                                {{date('Y-m-d h:i A',strtotime($serviceRequest->started_from_source))}}
                            </dd>
                            <dt class="col-sm-6">Drop Time :</dt>
                            <dd class="col-sm-6">{{date('Y-m-d h:i A',strtotime($serviceRequest->dropped_on_destination))}}</dd>
                            @endif
                        @else
                        @endif
                    </dl>
                        @if($param=='all')
                            @if($serviceRequest->request_status=='COMPLETED')

                            <dl class="row">
                            <dt class="col-sm-6 font-weight-bold text-info" style="font-size: 18px;">Trip Invoice</dt><dd class="col-sm-6"></dd>

                            <dt class="col-sm-6">Promo Code :</dt>
                            <dd class="col-sm-6">
                                @if(isset($Invoice['break_up']['promo_code']))
                                {{$Invoice['break_up']['promo_code']}}
                                @else
                                --
                                @endif
                            </dd>

                            <dt class="col-sm-6">Promo Discount :</dt>
                            <dd class="col-sm-6">
                                {{$Invoice['currency']}} {{$Invoice['break_up']['promo_code_value']}}
                            </dd>

                            <dt class="col-sm-6">Base fare :</dt>
                            <dd class="col-sm-6">
                                {{$Invoice['currency']}} {{$Invoice['break_up']['basefare']}}
                            </dd>
                            <dt class="col-sm-6">Distance fare :</dt>
                            <dd class="col-sm-6">
                                {{$Invoice['currency']}} {{$Invoice['break_up']['distanceFare']}}
                            </dd>
                            <dt class="col-sm-6">Waiting time fee :</dt>
                            <dd class="col-sm-6">
                                {{$Invoice['currency']}} {{$Invoice['break_up']['waitTimeCost']}}
                            </dd>
                            <dt class="col-sm-6">Tax :</dt>
                            <dd class="col-sm-6">
                                {{$Invoice['currency']}} {{$Invoice['break_up']['tax']}}
                            </dd>
                            <dt class="col-sm-6">Total :</dt>
                            <dd class="col-sm-6 pb-0">
                                {{$Invoice['currency']}} {{$Invoice['break_up']['total']}}
                            </dd></dl>
                            @endif
                        @endif
                    <dl class="row">
                        @if($param=='all')
                            @if($serviceRequest->request_status=='COMPLETED')
                            <dt class="col-sm-6 font-weight-bold text-info" style="font-size: 18px;">Comments & Rating</dt>
                            <dd class="col-sm-6"></dd>
                            <dt class="col-sm-6">Passenger Rating :</dt>
                            <dd class="col-sm-6">{{ $serviceRequest->rating_by_driver }}</dd>
                            <dt class="col-sm-6">Comment to Passenger :</dt>
                            <dd class="col-sm-6">{{ $serviceRequest->comment_by_driver }}</dd>
                            <dt class="col-sm-6">Driver Rating :</dt>
                            <dd class="col-sm-6">{{ $serviceRequest->rating_by_passenger }}</dd>
                            <dt class="col-sm-6">Comment to Driver :</dt>
                            <dd class="col-sm-6">{{ $serviceRequest->comment_by_passenger }}</dd>
                            @endif
                        @endif

                    </dl>
                </div>
                <div class="col-md-6">
                    <div id="map"></div>
                </div>
            </div>
        </div>

    </div>
</div>
@endsection

@section('styles')
<style type="text/css">
    #map {
        height: 450px;
    }
</style>
@endsection

@section('scripts')
<script type="text/javascript">
    var map;
    var zoomLevel = 11;

    function initMap() {

        map = new google.maps.Map(document.getElementById('map'));
        var base_url = window.location.origin;
        var marker = new google.maps.Marker({
            map: map,
            icon: base_url+'/webservice/diaridesharing/public/asset/img/marker-start.png',
            anchorPoint: new google.maps.Point(0, -29)
        });



         var markerSecond = new google.maps.Marker({
            map: map,
            icon: base_url+'/webservice/diaridesharing/public/asset/img/marker-end.png',
            anchorPoint: new google.maps.Point(0, -29)
        });

        var bounds = new google.maps.LatLngBounds();
        var waypoints=[];

        <?php
        // "var javascript_array = '".json_encode($MultipleWayPoints)."';";
       // echo "var javascript_array = ". $js_array . ";\n";
        ?>
        //console.log("------------------------------------");
       // console.log(JSON.parse(javascript_array));
        // javascript_array=JSON.parse(javascript_array);
        // for(var i=0; i<Object.keys(javascript_array).length;i++){
        //    // console.log(javascript_array[i].latitude);
        //     waypoints.push({
        //         location:new google.maps.LatLng(javascript_array[i].w_latitude, javascript_array[i].w_longitude),
        //         stopover:true
        //     });
        // /*    new google.maps.Marker({
        //     map: map,
        //     icon: base_url+'/laraval/blisscars/asset/img/marker-end.png',
        //    anchorPoint: new google.maps.Point(0, -29),
        //     position:new google.maps.LatLng(javascript_array[i].w_latitude, javascript_array[i].w_longitude)
        //     });*/
        //     }


        source = new google.maps.LatLng({{ $serviceRequest->s_latitude }}, {{ $serviceRequest->s_longitude }});

        destination = new google.maps.LatLng({{ $serviceRequest->d_latitude }}, {{ $serviceRequest->d_longitude }});

        marker.setPosition(source);
        markerSecond.setPosition(destination);

        var directionsService = new google.maps.DirectionsService;
        var directionsDisplay = new google.maps.DirectionsRenderer({suppressMarkers: true, preserveViewport: true});
        directionsDisplay.setMap(map);

        directionsService.route({
            origin: source,
            waypoints:waypoints,
            destination: destination,
            travelMode: google.maps.TravelMode.DRIVING
        }, function(result, status) {
            if (status == google.maps.DirectionsStatus.OK) {
                console.log(result);
                directionsDisplay.setDirections(result);

                marker.setPosition(result.routes[0].legs[0].start_location);
                markerSecond.setPosition(result.routes[0].legs[0].end_location);
            }
        });

        @if($serviceRequest->provider && $serviceRequest->status != 'COMPLETED')
        var markerProvider = new google.maps.Marker({
            map: map,
            icon: base_url+"/webservice/diaridesharing/public/asset/img/marker-car.png",
            anchorPoint: new google.maps.Point(0, -29)
        });

        provider = new google.maps.LatLng({{ $serviceRequest->provider->latitude }}, {{ $serviceRequest->provider->longitude }});
        markerProvider.setVisible(true);
        markerProvider.setPosition(provider);
        console.log('Provider Bounds', markerProvider.getPosition());
        bounds.extend(markerProvider.getPosition());
        @endif

        bounds.extend(marker.getPosition());
        bounds.extend(markerSecond.getPosition());
        map.fitBounds(bounds);
    }

</script>
<script src="https://maps.googleapis.com/maps/api/js?key={{env('GOOGLE_MAP_KEY')}}&libraries=places&callback=initMap" async defer></script>
@endsection
