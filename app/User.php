<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use HasApiTokens,Notifiable;
    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token', 'created_at', 'updated_at', 'user_scope', 'login_by'
    ];

    public function passenger_profile()
    {
        return $this->hasOne('App\Model\Profiles\PassengersProfile', 'user_id', 'id');
    }  

    public function driver_profile()
    {
        return $this->hasOne('App\Model\Profiles\DriverProfiles', 'user_id', 'id');
    }

    public function admin_profile()
    {
        return $this->hasOne('App\Model\Profiles\AdminsProfile', 'user_id', 'id');
    }

    public function driver_service()
    {
        return $this->hasOne('App\Model\ServiceType\DriverServiceType', 'user_id', 'id');
    }

}
