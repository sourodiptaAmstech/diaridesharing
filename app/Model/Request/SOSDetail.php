<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class SOSDetail extends Model
{
	protected $table = 'sos_details';
    protected $primaryKey = 'sos_detail_id';
}
