<?php

namespace App\Model\Request;

use Illuminate\Database\Eloquent\Model;

class EstimatedFare extends Model
{
  
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'estimated_fare';
    protected $primaryKey = 'estimated_fare_id';
}
