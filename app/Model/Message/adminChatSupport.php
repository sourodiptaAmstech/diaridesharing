<?php

namespace App\Model\Message;

use Illuminate\Database\Eloquent\Model;

class adminChatSupport extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'admin_support';
    protected $primaryKey = 'message_id';


}
