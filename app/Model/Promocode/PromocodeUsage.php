<?php

namespace App\Model\Promocode;

use Illuminate\Database\Eloquent\Model;

class PromocodeUsage extends Model
{
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promocode_usages';
    protected $primaryKey = 'promocode_usages_id';

    public function passenger()
    {
        return $this->belongsTo('App\Model\Profiles\PassengersProfile', 'user_id', 'user_id');
    }
}
