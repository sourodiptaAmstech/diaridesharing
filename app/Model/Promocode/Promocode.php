<?php

namespace App\Model\Promocode;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Promocode extends Model
{
	use SoftDeletes;
     /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'promocodes';
    protected $primaryKey = 'promocodes_id';


    protected $fillable = [
        'promo_code',
        'discount',
        'currency',
        'expiration',
        'status',
        'content',
        'user_type',
        'driver_discout_criteria_time',
        'driver_discout_criteria_distance',
        'activated_fordriver'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = ['deleted_at'];

}
