<?php

namespace App\Model\Transaction;

use Illuminate\Database\Eloquent\Model;

class UserTrasactionEmail extends Model
{
    //
   // use Notifiable;
    //
     /**
     * The table associated with the model.
     *
     * @var string
     */

    protected $table = 'user_transaction_email';
    protected $primaryKey = 'ute_id';
}
