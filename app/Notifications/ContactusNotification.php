<?php

namespace App\Notifications;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ContactusNotification extends Notification
{
    use Queueable;

    protected $msg1;
    protected $msg2;
    protected $msg3;
    protected $msg4;
    protected $email_subject;

    public function __construct($msg1,$msg2,$msg3,$msg4,$sub)
    {
        $this->msg1 = $msg1;
        $this->msg2 = $msg2;
        $this->msg3 = $msg3;
        $this->msg4 = $msg4;
        $this->email_subject = $sub;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
                    ->subject($this->email_subject)
                    ->line($this->msg1)
                    ->line($this->msg2)
                    ->line($this->msg3)
                    ->line($this->msg4)
                    ->line('Thank you for using our application!');
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
