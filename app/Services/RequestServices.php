<?php

namespace App\Services;

use App\Model\Request\ServiceRequest;
use App\Model\Request\ServiceRequestLog;
use Illuminate\Support\Facades\DB;
use Storage;

class RequestServices
{
    private function insertIntoServiceRequestLog($data){
        $ServiceRequestLog=new ServiceRequestLog();
        $ServiceRequestLog->request_id=$data->request_id;
        $ServiceRequestLog->passenger_id=$data->passenger_id;
        $ServiceRequestLog->driver_id=$data->driver_id;
        $ServiceRequestLog->service_type_id=$data->service_type_id;
        $ServiceRequestLog->request_type=$data->request_type;
        $ServiceRequestLog->status="REQUESTED";
        $ServiceRequestLog->save();
        return $ServiceRequestLog;
    }

    private function getSerReqLog($data){
        $ServiceRequestLog=ServiceRequestLog::where("request_id",$data->request_id)->where("service_type_id",$data->service_type_id)->get()->toArray();
        return $ServiceRequestLog;
    }

    private function getSerReqLogBYDriver($data){
        //->where("service_type_id",$data->service_type_id)
        $ServiceRequestLog=ServiceRequestLog::where("status",$data->status)->where("driver_id",$data->driver_id)->get()->toArray();
        return $ServiceRequestLog;
    }

    private function updateStatus($data,$status="REQUESTED"){
        $ServiceRequestLog=ServiceRequestLog::where("request_id",$data->request_id)->where("driver_id",$data->driver_id)->where("status",$status)->first();
        $ServiceRequestLog->status=$data->status;
       // print_r($ServiceRequestLog); exit;
        return $ServiceRequestLog->save();
    }

    private function getAllActiveSchedule($data){
        return ServiceRequest::where("request_type","SCHEDULE")->where("request_status","SERVICESEARCH")->whereBetween('schedule_datetime', [$data->currentDateTime, $data->addedOneMin])->get()->toArray();

    }
    private function getAllCompleteRequestByDriverId($data){
        if(isset($data->request_id)){
            return ServiceRequest::select("request_id","request_no","passenger_id","driver_id","payment_method","payment_status","request_type","service_type_id","rating_by_driver","comment_by_driver","accepted_on",
            "started_from_source","dropped_on_destination","isFemaleFriendly","schedule_datetime","route_key","static_map","created_at","request_status")
            ->where("request_id",$data->request_id)->where("driver_id",$data->user_id)->orderBy('request_id', 'desc')->get()->toArray();
        }
        else{
            return ServiceRequest::join('transaction_log as srl', 'srl.request_id','=','service_requests.request_id')->select("service_requests.request_id",
            "service_requests.request_no","service_requests.passenger_id","service_requests.driver_id","service_requests.payment_method",
            "srl.status as payment_status","service_requests.request_type","service_requests.service_type_id","service_requests.rating_by_passenger",
            "service_requests.comment_by_passenger","service_requests.accepted_on","service_requests.created_at","service_requests.started_from_source",
            "service_requests.dropped_on_destination","service_requests.isFemaleFriendly","service_requests.schedule_datetime","service_requests.route_key",
            "service_requests.static_map","service_requests.request_status")
            ->where(function($q){
                $q->where('service_requests.request_status', 'COMPLETED')
                ->orWhere('service_requests.request_status', 'CANCELBYDRIVER')
                ->orWhere('service_requests.driver_rating_status', 'GIVEN');
            })
            ->where("service_requests.driver_id",$data->user_id)->orderBy('request_id', 'desc')
            ->paginate(10)->toArray();
        }
    }
    private function getAllCompleteRequestByPassengerId($data){
        if(isset($data->request_id)){
            return ServiceRequest::select("request_id","request_no","passenger_id","driver_id","payment_method","payment_status","request_type","service_type_id","rating_by_passenger","comment_by_passenger","accepted_on",
            "started_from_source","dropped_on_destination","isFemaleFriendly","schedule_datetime","route_key","static_map","request_status","created_at")
            ->where("request_id",$data->request_id)
            ->where("passenger_id",$data->user_id)->orderBy('request_id', 'desc')->get()->toArray();
        }
        else{
            return ServiceRequest::join('transaction_log as srl', 'srl.request_id','=','service_requests.request_id')->select("service_requests.request_id","service_requests.request_no","service_requests.passenger_id","service_requests.driver_id",
            "service_requests.payment_method","srl.status as payment_status","service_requests.request_type","service_requests.service_type_id",
            "service_requests.rating_by_passenger","service_requests.comment_by_passenger","service_requests.accepted_on","service_requests.created_at",
            "service_requests.started_from_source","service_requests.dropped_on_destination","service_requests.isFemaleFriendly","service_requests.schedule_datetime",
            "service_requests.route_key","service_requests.static_map","service_requests.request_status")
            ->where(function($q){
                $q->where('service_requests.request_status', 'COMPLETED')
                ->orWhere('service_requests.request_status', 'CANCELBYPASSENGER')
                ->orWhere('service_requests.passenger_rating_status', 'GIVEN');
            })
            ->where("service_requests.passenger_id",$data->user_id)->orderBy('request_id', 'desc')
            ->paginate(10)->toArray();
        }
    }

    private function getAllUpComingSchuduleRequestByPassengerId($data){

        return ServiceRequest::select("request_id","request_no","passenger_id","driver_id","payment_method","payment_status","request_type","service_type_id","rating_by_driver","comment_by_driver","accepted_on",
        "started_from_source","dropped_on_destination","isFemaleFriendly","schedule_datetime","route_key","static_map")->where("request_type","SCHEDULE")->where("request_status","SERVICESEARCH")->where("passenger_id",$data->user_id)->orderBy('request_id', 'desc')
        ->paginate(10)->toArray();
    }
    public function accessInsertSerReqLog($data){
        return $this->insertIntoServiceRequestLog($data);
    }

    public function accessGetSerReqLog($data){
        return $this->getSerReqLog($data);
    }



    public function accessGetSerReqLogBYDriver($data){
        return $this->getSerReqLogBYDriver($data);

    }
    public function accessUpdateStatus($data,$status="REQUESTED"){
       // print_r($data); exit;
        return $this->updateStatus($data,$status);
    }

    public function accessGetAllActiveSchedule($data){
        return $this->getAllActiveSchedule($data);
    }

    public function accessGetAllCompleteRequestByDriverId($data){
        return $this->getAllCompleteRequestByDriverId($data);
    }
    public function accessGetAllCompleteRequestByPassengerId($data){
        return $this->getAllCompleteRequestByPassengerId($data);
    }
    public function accessGetAllUpComingSchuduleRequestByPassengerId($data){
        return $this->getAllUpComingSchuduleRequestByPassengerId($data);
    }



}
