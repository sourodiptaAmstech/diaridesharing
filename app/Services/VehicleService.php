<?php

namespace App\Services;

use App\Model\Vehicle\Vehicle;
use Illuminate\Support\Facades\DB;

class VehicleService
{
    private function create($data){
        return true;
    }
    private function update($data){
        return true;

    }
    private function get(){
         try{
            $vehicle=Vehicle::whereIn('make', ['Toyota', 'Honda', 'Hundai','Nissan'])->orderBy('year', 'DESC')->get()->toArray();
            if(!empty($vehicle))
            return ['message'=>trans("api.SYSTEM_MESSAGE.MAKE_FOUND"),"data"=>$vehicle,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>200];
            else
            return ['message'=>trans("api.SYSTEM_MESSAGE.MAKE_NOT_FOUND"),"data"=>[],"errors"=>array("exception"=>["No Content"],"error"=>[]),"statusCode"=>404];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e),"statusCode"=>500];
        }
        catch (Exception $e) {
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
        }
    }
    private function getMake(){
        try{

            $vehicle=DB::select("SELECT DISTINCT make FROM vehicle_master where make IN('Toyota', 'Honda', 'Hundai','Nissan') order by make DESC");

            if(!empty($vehicle))
           return ['message'=>trans("api.SYSTEM_MESSAGE.MAKE_FOUND"),"data"=>$vehicle,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>200];
           else
           return ['message'=>trans("api.SYSTEM_MESSAGE.MAKE_NOT_FOUND"),"data"=>[],"errors"=>array("exception"=>["No Content"],"error"=>[]),"statusCode"=>404];
       }
       catch(\Illuminate\Database\QueryException  $e){
           return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e),"statusCode"=>500];
       }
       catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
           return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e),"statusCode"=>500];
       }
       catch (Exception $e) {
           return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
       }
   }

   private function getModel($data){
    try{
        $vehicle=DB::select("SELECT DISTINCT model FROM vehicle_master where make =? order by model DESC",[$data->make]);
       if(!empty($vehicle))
       return ['message'=>trans("api.SYSTEM_MESSAGE.MODEL_FOUND"),"data"=>$vehicle,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>200];
       else
       return ['message'=>trans("api.SYSTEM_MESSAGE.MODEL_NOT_FOUND"),"data"=>[],"errors"=>array("exception"=>["No Content"],"error"=>[]),"statusCode"=>404];
   }
   catch(\Illuminate\Database\QueryException  $e){
       return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e),"statusCode"=>500];
   }
   catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
       return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e),"statusCode"=>500];
   }
   catch (Exception $e) {
       return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
   }
}

private function getYear($data){
    try{
      // $vehicle=Vehicle::select("id","year")->where('make', $data->make)->where('model', $data->model)->orderBy('year', 'DESC')->groupBy('year')->get()->toArray();
       $vehicle=DB::select("SELECT DISTINCT year FROM vehicle_master where make =? and model=? order by year DESC",[$data->make,$data->model]);
       if(!empty($vehicle))
       return ['message'=>trans("api.SYSTEM_MESSAGE.MODEL_YEAR_FOUND"),"data"=>$vehicle,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"error"=>[]),"statusCode"=>200];
       else
       return ['message'=>trans("api.SYSTEM_MESSAGE.MODEL_YEAR_NOT_FOUND"),"data"=>[],"errors"=>array("exception"=>["No Content"],"error"=>[]),"statusCode"=>404];
   }
   catch(\Illuminate\Database\QueryException  $e){
       return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e),"statusCode"=>500];
   }
   catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
       return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")],"e"=>$e),"statusCode"=>500];
   }
   catch (Exception $e) {
       return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>["Internal Server Error"],"e"=>$e),"statusCode"=>500];
   }
}

public function accessCreate($data){
    return $this->create($data);
}
public function accessGet(){
    return $this->get();
}
public function accessUpdate($data){
    return $this->update($data);
}
public function accessGetMake(){
    return $this->getMake();
}
public function accessGetModel($data){
    return $this->getModel($data);
}
public function accessGetYear($data){
    return $this->getYear($data);
}
}


