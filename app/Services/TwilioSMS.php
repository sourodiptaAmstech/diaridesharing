<?php
namespace App\Services;

class TwilioSMS
{
    private $twilio_id;
    private $twilio_token;
    private $authBase64;
    private $twilio_ph_no;
    function __construct() {
        $this->twilio_id = "ACb8013749f61820a670dfe9ad9d696c16";
        $this->twilio_token="dbe7b32f41ec7f12c759dce3d1081d4f";
        $this->authBase64=base64_encode($this->twilio_id.":".$this->twilio_token);
        $this->twilio_ph_no="+12044005564";
    }
    private function sendSMS($data){
        try{
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.twilio.com/2010-04-01/Accounts/$this->twilio_id/Messages.json",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_ENCODING => "",
                CURLOPT_MAXREDIRS => 10,
                CURLOPT_TIMEOUT => 0,
                CURLOPT_FOLLOWLOCATION => true,
                CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                CURLOPT_CUSTOMREQUEST => "POST",
                CURLOPT_USERPWD=>"$this->twilio_id:$this->twilio_token",
                CURLOPT_HTTPAUTH=>CURLAUTH_BASIC,
                CURLOPT_POSTFIELDS => "Body=$data->body&From=$this->twilio_ph_no&To=$data->isdCode.$data->mobile_no",
                CURLOPT_HTTPHEADER => array(
                    "Content-Type: application/x-www-form-urlencoded",
                    "Authorization: Basic $this->authBase64"
                ),
            ));
            $response = curl_exec($curl);
            $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
            curl_close($curl);
            // print_r($httpcode);
            $response=json_decode($response,true);
            $msg="OTP sent successfully!";
            if($httpcode!==201){
                $msg= $response['message'];
            }
            return ['message'=>$msg,"data"=>(object)[],"errors"=>array("exception"=>["Exception From Twilo"],"error"=>[]),"statusCode"=>$httpcode];
        }
        catch (Exception $e) {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Exception From Twilo"],"error"=>$e),"statusCode"=>500];
        }
    }
    public function accessSendSMS($data){
        return $this->sendSMS($data);
    }
}
