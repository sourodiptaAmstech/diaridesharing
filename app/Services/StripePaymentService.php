<?php

namespace App\Services;

use App\Model\ServiceType\MstServiceType;
use App\Model\Setting\Setting;
use App\Model\Payment\UserCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Services\DriversProfileService;
use App\Model\Profiles\DriverProfiles;
use App\Model\Profiles\PassengersProfile;


class StripePaymentService
{
    private $secret_key = 'sk_test_51IcWJkLbatUvb192IN9UV0KtxqFgkbzEs5ZxPfxFwCvcOGWhlmxuVBq3v9u0SreHgDcVtAO8oEADazZ9QXNQFUdm00S7CPsBXA';

    private $stripe;
    public function __construct(){
        
        // $Setting=Setting::where("key","stripe_mode")->select("value")->first();

        // if($Setting->value="demo"){
        //    $stripe_secret_key=Setting::where("key","stripe_secret_key_test")->select("value")->first();
        // }
        // else{
        //    $stripe_secret_key=Setting::where("key","stripe_secret_key")->select("value")->first();
        // }
        
        // $this->stripe = \Stripe\Stripe::setApiKey($stripe_secret_key->value);
        
    }

    private function curlMethods($data){
        try{
            $curl = curl_init();

                if($data->method=="POST"){
                    curl_setopt_array($curl,array(
                        CURLOPT_URL => $data->url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => $data->method,
                        CURLOPT_POSTFIELDS=>'source='.json_decode($data->CURLOPT_POSTFIELDS)->source,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Bearer ".$this->secret_key,
                            "Content-Type: application/x-www-form-urlencoded"
                    )));
                } elseif ($data->method=="DELETE") {
                    curl_setopt_array($curl, array(
                        CURLOPT_URL => $data->url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => '',
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => 'DELETE',
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Bearer ".$this->secret_key
                        )
                    ));
                } else {
                    curl_setopt_array($curl,array(
                        CURLOPT_URL => $data->url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => $data->method,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Bearer ".$this->secret_key
                        )));
                }

                $response = curl_exec($curl);
                
                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                $response=json_decode($response,true);
                return ['message'=>"","data"=>$response,"errors"=>"",'statusCode'=>$httpcode];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
    }


    private function createCustomer($data){
        try{
            // $customer = \Stripe\Customer::create([
            //     'email' => $data->email_id,
            //     'name' => $data->first_name." ".$data->last_name
            // ]);

            //create cuatomer account
            $CURLOPT_POSTFIELDS=array(
                'email' => $data->email_id,
                'name' => $data->first_name." ".$data->last_name
            );
            
            $customer=$this->createAccountCurlMethod((object)[
                'url'=>'https://api.stripe.com/v1/customers',
                'CURLOPT_POSTFIELDS'=>$CURLOPT_POSTFIELDS
            ]);

            return ["message"=>"Stripe Account Created!!", "status"=>true,"Exception"=>false,"e"=>false,"stripe_cust_id"=>$customer['data']['id']];
         }
         catch(\Exception $e){
             return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$e];
         }
    }

    private function insertCardInfo($data){
       
        $CURLOPT_POSTFIELDS=array("source"=>$data->card_token);
        return $this->curlMethods((object)[
            'url'=>'https://api.stripe.com/v1/customers/'.$data->stripe_customer_id.'/sources',
            'method'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
    }

    private function addCard($data){
        try{
            //$customer = \Stripe\Customer::retrieve($data->stripe_customer_id);
            
            // $customer = $this->curlMethods((object)[
            //     'url'=>'https://api.stripe.com/v1/customers/'.$data->stripe_customer_id,
            //     'method'=>'GET'
            // ]);
            // // dd($customer['data']->deleted);
            // dd($customer->deleted);

            // if($customer->deleted==null){

                
            // }else{
            //     return ['message'=>"Account not Found!!","data"=>(object)[],"errors"=>array("exception"=>["stripe Account not Found!!"],"error"=>[]),"statusCode"=>404];
            // }
            $card = $this->insertCardInfo($data);
            
            $exist=UserCard::where('user_id',$data->user_id)
            ->where('last_four',$card['data']['last4'])
            ->where('brand',$card['data']['brand'])
            ->count();
        
            if($exist == 0){
               $create_card = new UserCard;
               $create_card->user_id = $data->user_id;
               $create_card->card_token = $card['data']['id'];
               $create_card->last_four = $card['data']['last4'];
               $create_card->brand = $card['data']['brand'];
               $create_card->is_default = 0;
               $create_card->save();
            }else{

                return ['message'=>trans("api.SYSTEM_MESSAGE.CARDADDED"),"data"=>[],"errors"=>array("exception"=>["Card Already Added"],"error"=>[]),"statusCode"=>400];
            }
            // set default card
            $findCardAll =UserCard::where("user_id",$data->user_id)->get()->toArray();
            if(count($findCardAll)===1){
                $create_card->is_default =1;
                $create_card->save();
            }

            return ['message'=>trans("api.SYSTEM_MESSAGE.CARDADD"),"data"=>$findCardAll,"errors"=>array("exception"=>["Resource Created"],"error"=>[]),"statusCode"=>201];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Cannot able to add card!","data"=>(object)[],"errors"=>array("exception"=>["stripe resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }

    private function listCard($data){
        try{
            $cards=UserCard::where('user_id',$data->user_id)->orderBy('created_at','desc')->get()->toArray();

            if(!empty($cards)){
                return ['message'=>"Card List","data"=>$cards,"errors"=>array("exception"=>["card found"],"error"=>[]),"statusCode"=>200];
            } else {
                return ['message'=>trans("api.SYSTEM_MESSAGE.NOCARDADD"),"data"=>[],"errors"=>array("exception"=>["card resource not found"],"error"=>[]),"statusCode"=>404];
            }
            
            // if($data->stripe_customer_id!=null || $data->stripe_customer_id!="" )
            // {
            //     $stripe = new \Stripe\StripeClient($this->secret_key);

            //     $cards=$stripe->customers->allSources($data->stripe_customer_id,['object' => 'card']);

            //     return ['message'=>"Card List","data"=>$cards['data'],"errors"=>array("exception"=>["card found"],"error"=>[]),"statusCode"=>200];
            // } else {
            //     return ['message'=>trans("api.SYSTEM_MESSAGE.NOCARDADD"),"data"=>[],"errors"=>array("exception"=>["card resource not found"],"error"=>[]),"statusCode"=>404];
            // }

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to fetch your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found. Please add card.","data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }

    private function cardDeleteCurl($data){
        return $this->curlMethods((object)[
            'url'=>'https://api.stripe.com/v1/customers/'.$data->stripe_customer_id.'/sources/'.$data->card_token,
            'method'=>'DELETE'
            ]);
    }


    private function deleteCard($data){
        try{
            //$customer = \Stripe\Customer::retrieve($data->stripe_customer_id);
            $this->cardDeleteCurl($data);
            //$customer->sources->retrieve($data->card_token)->delete();
            UserCard::where('card_token',$data->card_token)->delete();
            return ['message'=>trans("api.SYSTEM_MESSAGE.CARD_DELETE"),"data"=>[],"errors"=>array("exception"=>["card deleted"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to delete your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to delete your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["card resource not found"],"error"=>$e),"statusCode"=>404];
        }
    }
    

    private function defaultCardSetForStripe($data) {
        $service_url = 'https://api.stripe.com/v1/customers/' . $data->stripe_customer_id;
        $curl_post_data = array(
            'default_source' => $data->card_token
        );
        $headers = array(
            'Authorization: Bearer '.$this->secret_key
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_URL, $service_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($curl_post_data));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        $output = curl_exec($ch);
        curl_close($ch);
        $output = json_decode($output, true);
        if (empty($output['id'])) {
            $result = array(
                'status' => 0,
                'msg' => $output['error']['message']
            );
        } else {
            $result = array(
                'status' => 1,
                'msg' => 'Done.',
                'data' => ''
            );
        }
        return $result;
    }


    private function setDefaultCard($data){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $data->user_id)->update(array('is_default' => 0));
            $UserCardSetDefault=UserCard::where('card_token', $data->card_token)->update(array('is_default' => 1));

            $this->defaultCardSetForStripe($data);

            return response(['message'=>trans("api.SYSTEM_MESSAGE.DEFAUTL_CARD"),"data"=>[],"errors"=>array("exception"=>["Card Updated"])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to update your card. Please try later","data"=>(object)[],"errors"=>array("exception"=>["update fail"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"No card found.","data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }
    }

    private function createAccountCurlMethod($data){
        $headers = array(
            'Authorization: Bearer '.$this->secret_key
        );

        $curl = curl_init();
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_URL, $data->url);
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data->CURLOPT_POSTFIELDS));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

        $response = curl_exec($curl);
        $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
        curl_close($curl);
        $response=json_decode($response,true);

        return ['message'=>"","data"=>$response,"errors"=>"",'statusCode'=>$httpcode];
    }

    private function createDriverAccount($data){
        try{
            $DriversProfileService = new DriversProfileService();
            $driverProfile = $DriversProfileService->accessGetProfile($data);

            if($driverProfile["data"]->stripe_account_id==null||$driverProfile["data"]->stripe_account_id==""){
                //create account
                $CURLOPT_POSTFIELDS=array(
                    "type"=>"express",
                    "country"=>$driverProfile["data"]->country_iso,
                    "email"=>$driverProfile["data"]->email_id,
                    "capabilities[card_payments][requested]"=>"true",
                    "capabilities[transfers][requested]"=>"true"
                );
                
                $createAccount=$this->createAccountCurlMethod((object)[
                    'url'=>'https://api.stripe.com/v1/accounts',
                    'CURLOPT_POSTFIELDS'=>$CURLOPT_POSTFIELDS
                    ]);
                
                if($createAccount["statusCode"]==400){
                    return ['message'=>trans("api.SYSTEM_MESSAGE.STRIPE_ACCOUNT_NOT_CREATE"),"data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>[]),"statusCode"=>400];
                }
                //save stripe account id
                $DriverProfiles = DriverProfiles::where('user_id',$data->user_id)->first();
                $DriverProfiles->stripe_account_id = $createAccount['data']['id'];
                $DriverProfiles->save();

                //create accountLinks
                $CURLOPT_POSTFIELDS=array(
                    "account"=>$createAccount['data']['id'],
                    "refresh_url"=>"https://dianicolau.com/webservice/diaridesharing/public/refresh/account/".$data->user_id,
                    "return_url"=>"https://dianicolau.com/webservice/diaridesharing/public/create/account/success",
                    "type"=>"account_onboarding"
                );
                
                $createAccountLink=$this->createAccountCurlMethod((object)[
                    'url'=>'https://api.stripe.com/v1/account_links',
                    'CURLOPT_POSTFIELDS'=>$CURLOPT_POSTFIELDS
                ]);
                
                return ['message'=>$createAccountLink['message'],"data"=>$createAccountLink['data'],"errors"=>array("exception"=>["Account created"]),"statusCode"=>$createAccountLink['statusCode']];

            } else {
                //retrieve account
                $getAccount = $this->curlMethods((object)[
                    'url'=>'https://api.stripe.com/v1/accounts/'.$driverProfile["data"]->stripe_account_id,
                    'method'=>'GET'
                ]);
                if($getAccount["data"]["charges_enabled"]==true){
                    //create Login Link
                    $createLoginLink=$this->createAccountCurlMethod((object)[
                        'url'=>'https://api.stripe.com/v1/accounts/'.$driverProfile["data"]->stripe_account_id.'/login_links',
                        'CURLOPT_POSTFIELDS'=>[]
                    ]);
                    
                    return ['message'=>$createLoginLink['message'],"data"=>$createLoginLink['data'],"errors"=>array("exception"=>["Account created"]),"statusCode"=>$createLoginLink['statusCode']];

                } else {
                    //create accountLinks
                    $CURLOPT_POSTFIELDS=array(
                        "account"=>$driverProfile["data"]->stripe_account_id,
                        "refresh_url"=>"https://dianicolau.com/webservice/diaridesharing/public/refresh/account/".$data->user_id,
                        "return_url"=>"https://dianicolau.com/webservice/diaridesharing/public/create/account/success",
                        "type"=>"account_onboarding"
                    );
                    
                    $createAccountLink=$this->createAccountCurlMethod((object)[
                        'url'=>'https://api.stripe.com/v1/account_links',
                        'CURLOPT_POSTFIELDS'=>$CURLOPT_POSTFIELDS
                    ]);
                    
                    return ['message'=>$createAccountLink['message'],"data"=>$createAccountLink['data'],"errors"=>array("exception"=>["Account created"]),"statusCode"=>$createAccountLink['statusCode']];
                }

            }

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to create an account!","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to create an account!","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Cannot able to create an account!","data"=>(object)[],"errors"=>array("exception"=>["stripe resource not found"],"error"=>$e),"statusCode"=>404];
        }

    }

     private function refreshDriverAccount($data){
        try{
            $DriversProfileService = new DriversProfileService();
            $driverProfile = $DriversProfileService->accessGetProfile($data);
            //retrieve account
            $getAccount = $this->curlMethods((object)[
                'url'=>'https://api.stripe.com/v1/accounts/'.$driverProfile["data"]->stripe_account_id,
                'method'=>'GET'
            ]);

            if($getAccount["data"]["charges_enabled"]==true){
                //create Login Link
                $createLoginLink=$this->createAccountCurlMethod((object)[
                    'url'=>'https://api.stripe.com/v1/accounts/'.$driverProfile["data"]->stripe_account_id.'/login_links',
                    'CURLOPT_POSTFIELDS'=>[]
                ]);
                
                return ['message'=>$createLoginLink['message'],"data"=>$createLoginLink['data'],"errors"=>array("exception"=>["Account created"]),"statusCode"=>$createLoginLink['statusCode']];

            } else {
                //create accountLinks
                $CURLOPT_POSTFIELDS=array(
                    "account"=>$driverProfile["data"]->stripe_account_id,
                    "refresh_url"=>"https://dianicolau.com/webservice/diaridesharing/public/refresh/account/".$data->user_id,
                    "return_url"=>"https://dianicolau.com/webservice/diaridesharing/public/create/account/success",
                    "type"=>"account_onboarding"
                );
                
                $createAccountLink=$this->createAccountCurlMethod((object)[
                    'url'=>'https://api.stripe.com/v1/account_links',
                    'CURLOPT_POSTFIELDS'=>$CURLOPT_POSTFIELDS
                ]);
                
                return ['message'=>$createAccountLink['message'],"data"=>$createAccountLink['data'],"errors"=>array("exception"=>["Account created"]),"statusCode"=>$createAccountLink['statusCode']];
            }

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>"Cannot able to create an account!","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>"Cannot able to create an account!","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>"Cannot able to create an account!","data"=>(object)[],"errors"=>array("exception"=>["stripe resource not found"],"error"=>$e),"statusCode"=>404];
        }

    }

    private function getDriverStripeAccount($data){
        try{
            $DriversProfileService = new DriversProfileService();
            $driverProfile = $DriversProfileService->accessGetProfile($data);

            if($driverProfile["data"]->stripe_account_id==null||$driverProfile["data"]->stripe_account_id==""){

                return ['message'=>trans("api.SYSTEM_MESSAGE.STRIPE_ACCOUNT_NOT_FOUND"),"data"=>(object)[],"errors"=>array("exception"=>["No account found"],"error"=>[]),"statusCode"=>201];

            } else {
                //retrieve account
                $getAccount = $this->curlMethods((object)[
                    'url'=>'https://api.stripe.com/v1/accounts/'.$driverProfile["data"]->stripe_account_id,
                    'method'=>'GET'
                ]);

                $accountData=[];

                if($getAccount["data"]["charges_enabled"]==false){

                    if ($getAccount["data"]["requirements"]["disabled_reason"]=="rejected.fraud"||$getAccount["data"]["requirements"]["disabled_reason"]=="rejected.terms_of_service"||$getAccount["data"]["requirements"]["disabled_reason"]=="rejected.listed"||$getAccount["data"]["requirements"]["disabled_reason"]=="rejected.other") {
                        //set null stripe account id
                        $Profile=DriverProfiles::where("user_id",$data->user_id)->first();
                        $Profile->stripe_account_id = null;
                        $Profile->save();

                        return ['message'=>trans("api.SYSTEM_MESSAGE.STRIPE_ACCOUNT_NOT_FOUND"),"data"=>(object)[],"errors"=>array("exception"=>["No account found"],"error"=>[]),"statusCode"=>201];
                    }
                    elseif ($getAccount["data"]["requirements"]["disabled_reason"]=="action_required.requested_capabilities") {

                        if(count($getAccount["data"]["external_accounts"]["data"])>0){
                            $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                            $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];
                        } else {
                            $accountData["accountObj"]["bankName"] = "";
                            $accountData["accountObj"]["last4"] = "";
                        }

                        $accountData["status"] = "REQUESTED_CAPABILITIES";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.REQUESTED_CAPABILITIES");
                        
                    }
                    elseif ($getAccount["data"]["requirements"]["disabled_reason"]=="requirements.past_due") {

                        if(count($getAccount["data"]["external_accounts"]["data"])>0){
                            $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                            $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];
                        } else {
                            $accountData["accountObj"]["bankName"] = "";
                            $accountData["accountObj"]["last4"] = "";
                        }
                        
                        $accountData["status"] = "PAST_DUE";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.PAST_DUE");

                    }
                    elseif ($getAccount["data"]["requirements"]["disabled_reason"]=="requirements.pending_verification") {

                        if(count($getAccount["data"]["external_accounts"]["data"])>0){
                            $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                            $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];
                        } else {
                            $accountData["accountObj"]["bankName"] = "";
                            $accountData["accountObj"]["last4"] = "";
                        }
                        
                        $accountData["status"] = "PENDING_VERIFICATION";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.PENDING_VERIFICATION");

                    }
                    elseif ($getAccount["data"]["requirements"]["disabled_reason"]=="listed") {

                        if(count($getAccount["data"]["external_accounts"]["data"])>0){
                            $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                            $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];
                        } else {
                            $accountData["accountObj"]["bankName"] = "";
                            $accountData["accountObj"]["last4"] = "";
                        }
                        
                        $accountData["status"] = "PROHIBITED";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.PROHIBITED");

                    }
                    elseif ($getAccount["data"]["requirements"]["disabled_reason"]=="under_review") {

                        if(count($getAccount["data"]["external_accounts"]["data"])>0){
                            $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                            $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];
                        } else {
                            $accountData["accountObj"]["bankName"] = "";
                            $accountData["accountObj"]["last4"] = "";
                        }
                        
                        $accountData["status"] = "UNDER_REVIEW";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.UNDER_REVIEW");

                    }
                    elseif ($getAccount["data"]["requirements"]["disabled_reason"]=="other") {

                        if(count($getAccount["data"]["external_accounts"]["data"])>0){
                            $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                            $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];
                        } else {
                            $accountData["accountObj"]["bankName"] = "";
                            $accountData["accountObj"]["last4"] = "";
                        }
                        
                        $accountData["status"] = "DISABLED";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.DISABLED");

                    } else {

                        if(count($getAccount["data"]["external_accounts"]["data"])>0){
                            $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                            $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];
                        } else {
                            $accountData["accountObj"]["bankName"] = "";
                            $accountData["accountObj"]["last4"] = "";
                        }
                        
                        $accountData["status"] = "PENDING";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.PENDING");

                    }


                } else {
                    if(count($getAccount["data"]["external_accounts"]["data"])>0){

                        $accountData["accountObj"]["bankName"] = $getAccount["data"]["external_accounts"]["data"][0]["bank_name"];
                        $accountData["accountObj"]["last4"] = $getAccount["data"]["external_accounts"]["data"][0]["last4"];

                    } else {
                        $accountData["accountObj"]["bankName"] = "";
                        $accountData["accountObj"]["last4"] = "";
                    }

                    if ($getAccount["data"]["payouts_enabled"] == true) {
                        $accountData["status"] = "ACTIVE";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.ACTIVE");

                    } else {
                        $accountData["status"] = "PENDING";
                        $accountData["status_msg"] = trans("api.SYSTEM_MESSAGE.PENDING");
                    }
                }

            }

            return ['message'=>"Stripe account data.","data"=>(object)$accountData,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function createStripeCharge($data){
        try{
            $Charge = \Stripe\Charge::create(array(
                "amount" => $data->cost*100,
                "currency" => "usd",
                "customer" => $data->stripe_customer_id,
                "card" => $data->card_id,
                "description" => $data->description,
                "receipt_email" => "sourodipta.guha@brainiuminfotech.com"
                  ));
            return ["message"=>"Stripe Charge Created!!", "status"=>true,"Exception"=>false,"e"=>false,"trans_id"=>$Charge['id']];
        }
         catch(\Exception $e){
             return ["message"=>"Cannot able to create stipe account for the given user", "status"=>false,"Exception"=>true,"e"=>$e];
         }
    }


    public function makeCardPaymentIntent($data,$requestedRide){
        try{

            $UserCardUnSetDefault=UserCard::where('user_id', $requestedRide->user_id)->where("user_cards_id",$requestedRide->user_cards_id)->first();

            $Profile=PassengersProfile::where("user_id",$requestedRide->user_id)->first();

            $CURLOPT_POSTFIELDS=array(
                "amount"=>((float)$data->cost-(float)$data->promo_code_value)*100,
                "currency"=>"aoa",
                "customer"=>$Profile->stripe_customer_id,
                "source"=>$UserCardUnSetDefault->card_token,
                "confirm"=>"true"
            );
            
            $makePayment=$this->createAccountCurlMethod((object)[
                'url'=>'https://api.stripe.com/v1/payment_intents',
                'CURLOPT_POSTFIELDS'=>$CURLOPT_POSTFIELDS
            ]);

            $data=[];$statuCode=200;
            // print_r($makePayment); exit;
            if($makePayment['statusCode']==200){
                if($makePayment['data']['status']=="succeeded"){
                    // verify transcation
                    //$verifyPayment= $PayStackServices->verifyTranscationByRefrence((object)['reference'=>$makePayment['data']['data']['reference']]);
                 // print_r($verifyPayment);
                 // exit;


                    //$gateWayFee=$verifyPayment['data']['data']['fees'];
                   // $refernceTrasaction=$makePayment['data']['data']['reference'];
                    $data=[
                        'reference'=>$makePayment['data']['charges']['data'][0]['balance_transaction'],
                        'fees'=>$makePayment['data']['charges']['data'][0]['application_fee_amount'],
                        'status'=>$makePayment['data']['status']
                    ];

                } else {
                    // verify transaction
                    //$verifyPayment= $PayStackServices->verifyTranscationByRefrence((object)['reference'=>$makePayment['data']['data']['reference']]);
                    $statuCode=400;
                }

            } else {
                $statuCode=400;
            }

            return ['message'=>"Payment","data"=>$data,"errors"=>array("exception"=>["Payment"]),"statusCode"=>$statuCode];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"error"=>$e),"statusCode"=>500];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>["Bad request"],"error"=>$e),"statusCode"=>500];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.cannot_make_payment_from_card"),"data"=>(object)[],"errors"=>array("exception"=>["No card found."],"error"=>$e),"statusCode"=>404];
        }
    }


    private function getStripeBalance(){
        try{
            $balance= $this->curlMethods((object)[
                'url'=>'https://api.stripe.com/v1/balance',
                'method'=>'GET'
            ]);

            return ['message'=>"Stripe account balance.","data"=>$balance['data'],"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    private function stripeTransferPayment($data){
        try{
            // $Profile=new DriversProfileService();
            // $driverProfile=$Profile->accessGetProfile($data);

            $CURLOPT_POSTFIELDS=array(
                "amount"=>$data->cost*100,
                "currency"=>"aoa",
                "destination"=>$data->stripe_account_id
            );
            
            $createTransfer=$this->createAccountCurlMethod((object)[
                'url'=>'https://api.stripe.com/v1/transfers',
                'CURLOPT_POSTFIELDS'=>$CURLOPT_POSTFIELDS
            ]);
            
            return ['message'=>"Stripe transfer payment","data"=>$createTransfer,"errors"=>array("exception"=>["Everything OK"],"error"=>[]),"statusCode"=>200];
        }
        catch(\Illuminate\Database\QueryException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.System_Error"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
    }

    public function accessStripeTransferPayment($data){
        return $this->stripeTransferPayment($data);
    }

    public function accessGetStripeBalance(){
        return $this->getStripeBalance();
    }
    public function accessGetDriverStripeAccount($data){
        return $this->getDriverStripeAccount($data);
    }

    public function accessRefreshDriverAccount($data){
        return $this->refreshDriverAccount($data);
    }

    public function accessCreateDriverAccount($data){
        return $this->createDriverAccount($data);
    }

    public function accessCreateCustomer($data){
        return $this->createCustomer($data);
    }

    public function accessAddCards($data){
        return $this->addCard($data);
    }

    public function accessListCard($data){
        return $this->listCard($data);
    }

    public function accessDeleteCard($data){
        return $this->deleteCard($data);
    }

    public function accessSetDefaultCard($data){
     return $this->setDefaultCard($data);
    }

    public function accessCreateStripeCharge($data){
        return $this->createStripeCharge($data);
    }
}



