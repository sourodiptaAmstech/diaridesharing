<?php
namespace App\Services;



use App\User;
use Exception;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\ModelNotFoundException;

use Illuminate\Support\Facades\DB;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;
use App\Model\Request\ServiceRequest;
use App\Services\TwilioSMS;
use phpDocumentor\Reflection\Types\Null_;
use Illuminate\Support\Facades\Log;
use App\Model\Transaction\TransactionLog;

class ServiceRequestService
{
      // Accessing the services
      private function createRequest($data){
          $ServiceRequest=new ServiceRequest();
          $ServiceRequest->request_no=$data->request_no;
          $ServiceRequest->passenger_id=$data->passenger_id;
          $ServiceRequest->driver_id=$data->driver_id;
          $ServiceRequest->request_type=$data->request_type;
          $ServiceRequest->route_key=$data->route_key;
          $ServiceRequest->static_map=$data->static_map;
          //$ServiceRequest->locationDetails=$data->locationDetails;
          //$ServiceRequest->service_preferences=$data->preferences;
          $ServiceRequest->service_type_id=0;

          $ServiceRequest->save();
          return $ServiceRequest;

      }

      private function getallRequestByCustomerPagewise($data){
          return ServiceRequest::join('transaction_log as srl', 'srl.request_id','=','service_requests.request_id')
        ->select("service_requests.request_status","service_requests.request_id","service_requests.request_no",
        "service_requests.payment_method","service_requests.card_id","service_requests.created_at")
        ->where(function($q){
            $q->where('service_requests.request_status', 'COMPLETED')
            ->orWhere('service_requests.request_status', 'CANCELBYPASSENGER');
        })
        ->where("service_requests.passenger_id",$data->user_id)->orderBy('request_id', 'desc')
        ->paginate(10)->toArray();
       /* $ServiceRequest=ServiceRequest::select("request_id","request_no","payment_method","card_id","dropped_on_destination")
          ->where("passenger_id",$data->user_id)->where("payment_status","COMPLETED")->orderBy('request_id', 'desc')
          ->paginate(10)->toArray();
          return $ServiceRequest; */
        }



        private function getallRequestByDriverPagewise($data){
            return   ServiceRequest::join('transaction_log', function($join)
            {
                $join->on('transaction_log.request_id','service_requests.request_id');
                $join->where('transaction_log.status','=','COMPLETED');
            })->select('service_requests.request_id','service_requests.request_no','service_requests.passenger_id','service_requests.driver_id', 'service_requests.card_id','service_requests.request_type','service_requests.request_status','service_requests.service_type_id',
            'service_requests.rating_by_driver','service_requests.comment_by_driver','service_requests.driver_rating_status','service_requests.rating_by_passenger','service_requests.comment_by_passenger',
            'service_requests.passenger_rating_status','service_requests.accepted_on','service_requests.started_from_source','service_requests.dropped_on_destination','service_requests.isFemaleFriendly',
            'service_requests.schedule_datetime','service_requests.route_key','service_requests.static_map','service_requests.updated_at as requestUpdatedTime', 'service_requests.created_at as requestCreatedTime',
            'transaction_log.transaction_log_id','transaction_log.ride_insurance','transaction_log.tax','transaction_log.commision_percentage','transaction_log.per_minute','transaction_log.per_distance_km',
            'transaction_log.minimum_waiting_time_in_minutes','transaction_log.waiting_charge_per_min','transaction_log.waitTime','transaction_log.duration_hr','transaction_log.duration_min',
            'transaction_log.duration_sec','transaction_log.duration','transaction_log.distance_km','transaction_log.distance_miles','transaction_log.distance_meters','transaction_log.cost','transaction_log.currency',
            'transaction_log.payment_method','transaction_log.types','transaction_log.commission','transaction_log.transaction_type','transaction_log.is_paid','transaction_log.promo_code','transaction_log.promo_code_value','transaction_log.promocode_usages_id',
            'transaction_log.updated_at as transactionUpdatedDateTime', 'transaction_log.created_at as transactionCreatedTime','transaction_log.status')
            ->where("service_requests.payment_status",'=',"COMPLETED")
            ->where("service_requests.request_status",'=',"COMPLETED")
            ->where("service_requests.driver_id",'=',$data->user_id)
          /*  ->union(TransactionLog::join('service_requests',function($joinTwo){
                $joinTwo->on('service_requests.request_id','transaction_log.payment_gateway_transaction_id');
                $joinTwo->where("service_requests.request_status",'=',"COMPLETED");
                $joinTwo->where('transaction_log.status','=','COMPLETED');
            })
            ->select('service_requests.request_id','service_requests.request_no','service_requests.passenger_id','service_requests.driver_id', 'service_requests.card_id','service_requests.request_type','service_requests.request_status','service_requests.service_type_id',
            'service_requests.rating_by_driver','service_requests.comment_by_driver','service_requests.driver_rating_status','service_requests.rating_by_passenger','service_requests.comment_by_passenger',
            'service_requests.passenger_rating_status','service_requests.accepted_on','service_requests.started_from_source','service_requests.dropped_on_destination','service_requests.isFemaleFriendly',
            'service_requests.schedule_datetime','service_requests.route_key','service_requests.static_map','service_requests.updated_at as requestUpdatedTime', 'service_requests.created_at as requestCreatedTime',
            'transaction_log.transaction_log_id','transaction_log.ride_insurance','transaction_log.tax','transaction_log.commision_percentage','transaction_log.per_minute','transaction_log.per_distance_km',
            'transaction_log.minimum_waiting_time_in_minutes','transaction_log.waiting_charge_per_min','transaction_log.waitTime','transaction_log.duration_hr','transaction_log.duration_min',
            'transaction_log.duration_sec','transaction_log.duration','transaction_log.distance_km','transaction_log.distance_miles','transaction_log.distance_meters','transaction_log.cost','transaction_log.currency',
            'transaction_log.payment_method','transaction_log.types','transaction_log.commission','transaction_log.transaction_type','transaction_log.is_paid','transaction_log.promo_code','transaction_log.promo_code_value','transaction_log.promocode_usages_id',
            'transaction_log.updated_at as transactionUpdatedDateTime', 'transaction_log.created_at as transactionCreatedTime','transaction_log.status')
            ->where("transaction_log.transaction_type",'=',"CANCELBYPASSENGERCHARGE")
            ->where("transaction_log.status",'=',"COMPLETED")
            ->where("service_requests.driver_id",'=',$data->user_id)

            )*/
            ->union(
                ServiceRequest::join('transaction_log', function($join)
                {
                    $join->on('transaction_log.request_id','service_requests.request_id');
                    $join->where('transaction_log.transaction_type','=','CANCELBYDRIVERCHARGE');
                    $join->where('transaction_log.status','=',"PENDING");
                })->select('service_requests.request_id','service_requests.request_no','service_requests.passenger_id','service_requests.driver_id', 'service_requests.card_id','service_requests.request_type','service_requests.request_status','service_requests.service_type_id',
                'service_requests.rating_by_driver','service_requests.comment_by_driver','service_requests.driver_rating_status','service_requests.rating_by_passenger','service_requests.comment_by_passenger',
                'service_requests.passenger_rating_status','service_requests.accepted_on','service_requests.started_from_source','service_requests.dropped_on_destination','service_requests.isFemaleFriendly',
                'service_requests.schedule_datetime','service_requests.route_key','service_requests.static_map','service_requests.updated_at as requestUpdatedTime', 'service_requests.created_at as requestCreatedTime',
                'transaction_log.transaction_log_id','transaction_log.ride_insurance','transaction_log.tax','transaction_log.commision_percentage','transaction_log.per_minute','transaction_log.per_distance_km',
                'transaction_log.minimum_waiting_time_in_minutes','transaction_log.waiting_charge_per_min','transaction_log.waitTime','transaction_log.duration_hr','transaction_log.duration_min',
                'transaction_log.duration_sec','transaction_log.duration','transaction_log.distance_km','transaction_log.distance_miles','transaction_log.distance_meters','transaction_log.cost','transaction_log.currency',
                'transaction_log.payment_method','transaction_log.types','transaction_log.commission','transaction_log.transaction_type','transaction_log.is_paid','transaction_log.promo_code','transaction_log.promo_code_value','transaction_log.promocode_usages_id',
                'transaction_log.updated_at as transactionUpdatedDateTime', 'transaction_log.created_at as transactionCreatedTime','transaction_log.status')
                ->where("service_requests.payment_status",'=',"PENDING")
                ->where("service_requests.request_status",'=',"CANCELBYDRIVER")
                ->where("service_requests.driver_id",'=',$data->user_id)
            )->orderBy('request_id', 'desc')->paginate(10)->toArray();
        }



      private function updateRequestNo($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_no=$data->request_no;
        $ServiceRequest->save();
        return $ServiceRequest;
    }
    private function getRequestByID($data){
        //print_r($data->request_id); exit;
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        return $ServiceRequest;
    }

    private function updateRideStatus($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_status=$data->request_status;
        $ServiceRequest->service_type_id=$data->service_type_id;
        if(isset($data->payment_method)){
            if($data->payment_method!="" && $data->payment_method!=Null){
                $ServiceRequest->payment_method=$data->payment_method;
                //echo $data->isFemaleFriendly; exit;
            }
        }
        if(isset($data->card_id)){
            if($data->card_id!="" && $data->card_id!=Null){
                $ServiceRequest->card_id=$data->card_id;
                //echo $data->isFemaleFriendly; exit;
            }
        }
        // print_r($data); exit;
        if($data->isFemaleFriendly!="" && $data->isFemaleFriendly!=Null){
            $ServiceRequest->isFemaleFriendly=$data->isFemaleFriendly;
            //echo $data->isFemaleFriendly; exit;
        }
        $ServiceRequest->save();
        return $ServiceRequest;
    }
    private function setSchedule($data){
       // Log::info("in setSchedule");Log::info(json_encode($data));
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_status=$data->request_status;
        $ServiceRequest->save();
       // Log::info("on setSchedule");Log::info(json_encode($ServiceRequest));
        return $ServiceRequest;
    }
    private function updateDriver($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_status=$data->request_status;
        $ServiceRequest->driver_id=$data->driver_id;
        if($data->request_status==="ACCEPTED"){
            $ServiceRequest->accepted_on=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="STARTED"){
            $ServiceRequest->started_from_source=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="DROP"){
            $ServiceRequest->dropped_on_destination=date("Y-m-d H:i:s");
        }
        else if($data->request_status=="PAYMENT"){
            if($data->payment_method=="CASH"){
                   $ServiceRequest->payment_status=$data->payment_status;
            }

        }
        else if($data->request_status=="RATING"){
            $ServiceRequest->rating_by_driver=$data->rating_by_driver;
            $ServiceRequest->comment_by_driver=$data->comment_by_driver;
            $ServiceRequest->driver_rating_status=$data->driver_rating_status;
        }
        $ServiceRequest->save();
        if($data->request_status=="RATING"){
            $this->calculateOveralRatingPassenger($ServiceRequest);
        }
        return $ServiceRequest;
    }
    private function calculateOveralRatingPassenger($data){
        $sql='SELECT ROUND(avg(rating_by_driver),2) as overall_rating FROM service_requests where passenger_id="'.$data->passenger_id.'" and driver_rating_status="GIVEN"';
        $overAllRating=  DB::select($sql);
        $DriverProfiles=PassengersProfile::where("user_id",$data->passenger_id)->first();
        $DriverProfiles->overall_rating=$overAllRating[0]->overall_rating;
        $DriverProfiles->save();
        return true;


    }
    private function calculateOveralRatingDriver($data){
        $sql='SELECT ROUND(avg(rating_by_passenger),2) as overall_rating FROM service_requests where driver_id="'.$data->driver_id.'" and passenger_rating_status="GIVEN"';
        $overAllRating=  DB::select($sql);
        $DriverProfiles=DriverProfiles::where("user_id",$data->driver_id)->first();
        $DriverProfiles->overall_rating=$overAllRating[0]->overall_rating;
        $DriverProfiles->save();
        return true;
    }

    private function addPassengerRatingComment($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->rating_by_passenger=$data->rating_by_passenger;
        $ServiceRequest->comment_by_passenger=$data->comment_by_passenger;
        $ServiceRequest->passenger_rating_status=$data->passenger_rating_status;
        $ServiceRequest->save();
      //  print_r($ServiceRequest);
        // update over all rating
       $this->calculateOveralRatingDriver($ServiceRequest);






        return $ServiceRequest;
    }



    private function getRiderActiveRequests($data){

        $ServiceRequest=ServiceRequest::where("passenger_id",$data->user_id)
            ->where(function($sq){
                $sq->where("request_status","<>","SERVICESEARCH")
                ->where("request_status","<>","COMPLETED")
                ->where("request_status","<>","CANCELBYSYSTEM")
                ->where("request_status","<>","NOSERVICEFOUND")
                ->where("passenger_rating_status","PENDING")
                ->orWhere(function($sqls){
                    $sqls->where("request_status","COMPLETED")
                    ->where("request_status","<>","CANCELBYSYSTEM")
                    ->where("passenger_rating_status","PENDING");
                });
            })->get()->toArray();
           return $ServiceRequest;
        }

    private function checkOnRide($data){
       // Log::info("IN the server requesrt accessUpdateRideStatusSetSchedule");Log::info(json_encode($data));
        $ServiceRequest=ServiceRequest::where("passenger_id",$data->passenger_id)
        ->whereIn('request_status', ["RIDESEARCH", "ACCEPTED", "REACHED", "STARTED", "DROP", "PAYMENT", "RATING"])->get()->toArray();
       // Log::info("IN the server requesrt accessUpdateRideStatusSetSchedule");Log::info(json_encode($ServiceRequest));
        return $ServiceRequest;
    }

    private function autoCancelSchedule($data){
        $ServiceRequest=ServiceRequest::where("request_id",$data->request_id)->first();
        $ServiceRequest->request_status="CANCELBYSYSTEM";
        $ServiceRequest->save();
        return $ServiceRequest;
    }

    public function accessCreateRequest($data){
        return $this->createRequest($data);
    }
    public function accessUpdateRequestNo($data){
        return $this->updateRequestNo($data);
    }

    public function accessGetRequestByID($data){
        return $this->getRequestByID($data);
    }

    public function accessUpdateRideStatus($data){
        return $this->updateRideStatus($data);
    }

    public function accessUpdateRideStatusSetSchedule($data){
        //Log::info("IN the server requesrt accessUpdateRideStatusSetSchedule");Log::info(json_encode($data));
        $returns=$this->setSchedule($data);
        //Log::info("after setSchedule");Log::info(json_encode($returns));

        return $returns;
    }

    public function accessGetRiderActiveRequests($data){
        return $this->getRiderActiveRequests($data);
    }
    public function accessUpdateDriver($data){
        return $this->updateDriver($data);
    }
    public function accessAddPassengerRatingComment($data){
        return $this->addPassengerRatingComment($data);
    }


    public function accessCheckOnRide($data){
        return $this->checkOnRide($data);
    }

    public function accessAutoCancelSchedule($data){
        return $this->autoCancelSchedule($data);
    }

    public function accessGetallRequestByCustomerPagewise($data){
        return $this->getallRequestByCustomerPagewise($data);
    }

    public function accessGetallRequestByDriverPagewise($data){
        return $this->getallRequestByDriverPagewise($data);
    }
}
