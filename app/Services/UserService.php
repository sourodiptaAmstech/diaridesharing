<?php

namespace App\Services;

use App\User;
use Illuminate\Database\Eloquent\ModelNotFoundException;
class UserService
{
    private function createUser($data){
        $User = new User();
        $User->login_type=$data->login_by;
        if($data->login_by==="manual")
        $User->password=bcrypt(trim($data->password));
        else
        $User->social_unique_id =$data->social_unique_id;
        $User->user_scope=$data->user_scope;//"passenger-service";
        $User->username=$data->username;
        $User->save();
        return $User;
    }
    private function updatePassword($data){
        try{
            $User=User::where("id",$data->user_id)->firstOrFail();
            $User->password=bcrypt(trim($data->password));
            $User->save();
            return ['message'=>trans("api.SYSTEM_MESSAGE.Password_successfully_Changed"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>[]),"statusCode"=>200];

        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(ModelNotFoundException $e)
        {
            return ['message'=>trans("api.SYSTEM_MESSAGE.Not_Registered"),"data"=>(object)[],"errors"=>array("exception"=>["Model Not Found Exception"],"error"=>$e),"statusCode"=>403];
        }
       // return true;
    }
    private function updateUser($data){
        return true;
    }
    private function resetPasswordOTP($data){
        $User=User::where("id",$data->user_id)->update(["otp"=>$data->otp,"otp_created_on"=>$data->otpDate]);
        return $User;
    }
    private function getUsers($data){
        $User=User::where("id",$data->user_id)->first();
        return $User;
    }
    public function accessCreateUser($data){
        return $this->createUser($data);
    }
    public function accessUpdatePassword($data){
        return $this->updatePassword($data);
    }
    public function accessResetPasswordOTP($data){
        return $this->resetPasswordOTP($data);
    }
    public function accessGetUsers($data){
        return $this->getUsers($data);
    }

}
