<?php

namespace App\Services;

use App\Model\Setting\Setting;
use App\Model\Payment\UserCard;
use Illuminate\Database\Eloquent\ModelNotFoundException;

class PayStackServices
{
    public function __construct(){
        // $Setting=Setting::where("key","payment_gateway_mode")->select("value")->first();
    }
    private function curlMethods($data){
        try{
            $curl = curl_init();

                if($data->methord=="POST"){
                      curl_setopt_array($curl,array(
                    CURLOPT_URL => "https://api.paystack.co/".$data->url,
                    CURLOPT_RETURNTRANSFER => true,
                    CURLOPT_ENCODING => "",
                    CURLOPT_MAXREDIRS => 10,
                    CURLOPT_TIMEOUT => 0,
                    CURLOPT_FOLLOWLOCATION => true,
                    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                    CURLOPT_CUSTOMREQUEST => $data->methord,
                    CURLOPT_POSTFIELDS=>$data->CURLOPT_POSTFIELDS,
                    CURLOPT_HTTPHEADER => array(
                        "Authorization: Bearer sk_test_0dc62d04d51e546653fa63fcbcb7eebf85008ae7",
                        "Content-Type: application/json"
                    )));
                }
                else{
                    curl_setopt_array($curl,array(
                        CURLOPT_URL => "https://api.paystack.co/".$data->url,
                        CURLOPT_RETURNTRANSFER => true,
                        CURLOPT_ENCODING => "",
                        CURLOPT_MAXREDIRS => 10,
                        CURLOPT_TIMEOUT => 0,
                        CURLOPT_FOLLOWLOCATION => true,
                        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
                        CURLOPT_CUSTOMREQUEST => $data->methord,
                        CURLOPT_HTTPHEADER => array(
                            "Authorization: Bearer sk_test_0dc62d04d51e546653fa63fcbcb7eebf85008ae7"
                        )));
                }

                $response = curl_exec($curl);
               // print_r($response); exit;
                $httpcode = curl_getinfo($curl, CURLINFO_HTTP_CODE);
                curl_close($curl);
                $response=json_decode($response,true);
                return ['message'=>"","data"=>$response,"errors"=>"",'statusCode'=>$httpcode];
        }
        catch(\Illuminate\Database\QueryException  $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return ['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>["Bad Request"],"error"=>$e),"statusCode"=>400];
        }
    }

    public function verifyTranscationByRefrence($data){
        return $this->curlMethods((object)[
            'url'=>'transaction/verify/'.$data->reference,
            'methord'=>'GET'
            ]);
    }

    public function deleteCards($data){
        $CURLOPT_POSTFIELDS=array("authorization_code"=>$data->authorization_code);
        return $this->curlMethods((object)[
            'url'=>'customer/deactivate_authorization',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
    }

    public function makePayment($data){
       // print_r($data); exit;
        $CURLOPT_POSTFIELDS=array("authorization_code"=>$data->authorization_code,"email"=>$data->email, "amount"=>$data->amount);
        return $this->curlMethods((object)[
            'url'=>'transaction/charge_authorization',
            'methord'=>'POST',
            'CURLOPT_POSTFIELDS' =>json_encode($CURLOPT_POSTFIELDS)
            ]);
    }

}



