<?php

namespace App\Http\Controllers\Admin\Chat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use  App\Http\Controllers\Api\Driver\MessageController;
use Validator;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;


class ChatSupportController extends Controller
{
	public function passengerChatSupport(Request $request){
		$passengers = PassengersProfile::all();
		if(isset($request->param)){
			$arrayName=json_decode(decrypt($request->param));
			$pram='yes';
		}else{
			$arrayName='';
			$pram='';
		}
		return view('admin.chat.passenger',compact('passengers','arrayName','pram'));
	}

	public function driverChatSupport(){
		$drivers = DriverProfiles::all();
		return view('admin.chat.driver',compact('drivers'));
	}


	public function sendChatForSupport(Request $request)
	{
		try{
			$rule=[
	                'thread_id'=>'required_if:user_scope,admin-service',
	                'timeZone'=>'required',
	                'user_scope'=>'required|in:driver-service,passenger-service,admin-service',
	                'message'=>'required'
	            ];
	        $validator=$this->requestValidation($request->all(),$rule);
	        if($validator->status=="false"){
	            return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
	        };

	        $request->headers->set('timeZone', $request->timeZone);

			$MessageController = new MessageController();
			$messageSupport = $MessageController->sendMessageForSupport($request);

			return response(['message'=>"Message sent!","data"=>(object)[],"errors"=>[]],201);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }

	}


	public function getChatForSupport(Request $request)
	{
		try{
			$rule=[
	                'thread_id'=>'required_if:user_scope,admin-service',
	                'timeZone'=>'required',
	                'user_scope'=>'required|in:driver-service,passenger-service,admin-service',
	            ];
	        $validator=$this->requestValidation($request->all(),$rule);
	        if($validator->status=="false"){
	            return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422);
	        };

	        $request->headers->set('timeZone', $request->timeZone);

			$MessageController = new MessageController();
			$messageSupport = $MessageController->getMessageForSupport($request);

			return response(['message'=>"Messages","data"=>$messageSupport->original['data'],"errors"=>[]],201);
		}
		catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }

	}

	public function getDriver(Request $request){
		$user_id=$request->user_id;
		$driver = DriverProfiles::where('user_id',$user_id)->first();

		return response(['message'=>"Driver","data"=>$driver,"errors"=>[]],201);
	}

	public function getPassenger(Request $request){
		$user_id=$request->user_id;
		$passenger = PassengersProfile::where('user_id',$user_id)->first();

		return response(['message'=>"Passenger","data"=>$passenger,"errors"=>[]],201);
	}

}
