<?php

namespace App\Http\Controllers\Admin\Lease;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Leasing\Leasing;


class LeaseController extends Controller
{
    public function hourlyLease()
    {
        $leaseType='Hourly Lease';
        $leases = Leasing::join('service_types','leasing.services_type_id','=','service_types.id')
            ->join('passengers_profile','leasing.passenger_id','=','passengers_profile.user_id')
            ->select('leasing.*', 'service_types.name as service_name', 'passengers_profile.first_name as first_name','passengers_profile.last_name as last_name','passengers_profile.user_id as user_id')
            ->where('leasing.lease_type','hourly')
            ->orderBy('leasing.leasing_id', 'desc')
            ->get();
        if (isset($leases)){
            return view('admin.lease.lease',compact('leases','leaseType'));
        } else {
            return redirect()->back()->with('flash_error', 'Hourly lease not found!');
        }
    
    }

    public function dailyLease()
    {
        $leaseType='Daily Lease';
        $leases = Leasing::join('service_types','leasing.services_type_id','=','service_types.id')
            ->join('passengers_profile','leasing.passenger_id','=','passengers_profile.user_id')
            ->select('leasing.*', 'service_types.name as service_name', 'passengers_profile.first_name as first_name','passengers_profile.last_name as last_name','passengers_profile.user_id as user_id')
            ->where('leasing.lease_type','daily')
            ->orderBy('leasing.leasing_id', 'desc')
            ->get();

        if (isset($leases)){
            return view('admin.lease.lease',compact('leases','leaseType'));
        } else {
            return redirect()->back()->with('flash_error', 'Daily lease not found!');
        }

    }

    public function longTimeLease()
    {
        $leaseType='Long-Term Lease';
        $leases = Leasing::join('service_types','leasing.services_type_id','=','service_types.id')
            ->join('passengers_profile','leasing.passenger_id','=','passengers_profile.user_id')
            ->select('leasing.*', 'service_types.name as service_name', 'passengers_profile.first_name as first_name','passengers_profile.last_name as last_name','passengers_profile.user_id as user_id')
            ->where('leasing.lease_type','longTime')
            ->orderBy('leasing.leasing_id', 'desc')
            ->get();

        if (isset($leases)){
            return view('admin.lease.lease',compact('leases','leaseType'));
        } else {
            return redirect()->back()->with('flash_error', 'Long time lease not found!');
        }

    }

    public function showLeaseDetails($leasing_id,$param)
    {
        if ($param=='hourly') {
            $leaseType='Hourly Lease';
        }
        if ($param=='daily') {
            $leaseType='Daily Lease';
        }
        if ($param=='longTime') {
            $leaseType='Long-Term Lease';
        }
        
        $lease = Leasing::join('service_types','leasing.services_type_id','=','service_types.id')
            ->join('passengers_profile','leasing.passenger_id','=','passengers_profile.user_id')
            ->select('service_types.name as service_name','leasing.no_passenger','leasing.address','leasing.vehicle_equipment','leasing.services_type_id','leasing.to_datetime','leasing.from_datetime','leasing.dateOfJourny','leasing.details','leasing.leasing_id','passengers_profile.*')
            ->where('leasing.leasing_id',$leasing_id)
            ->first();
        
        return view('admin.lease.lease-details',compact('lease','param','leaseType'));

    }
}
