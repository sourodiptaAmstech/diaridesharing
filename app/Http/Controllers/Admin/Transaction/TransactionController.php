<?php

namespace App\Http\Controllers\Admin\Transaction;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\ServiceRequest;
use App\Model\Transaction\TransactionLog;
use App\Services\TransactionLogService;
use Illuminate\Support\Facades\DB;
use App\Services\PaymentService;
use App\Services\ServiceRequestService;
use App\Model\Profiles\PassengersProfile;

class TransactionController extends Controller
{
	public function index(Request $request,$id){
		$serviceRequest = DB::select('SELECT * from service_requests as sr
		left join transaction_log as tl on tl.request_id=sr.request_id
		where  sr.driver_id=? and tl.request_id<>""  and (sr.payment_status="COMPLETED" or (sr.payment_status="PENDING" and sr.request_status="CANCELBYDRIVER")) order by sr.request_id DESC',[$request->id]);
		if(count($serviceRequest)>0){
			foreach($serviceRequest as $key=>$value)
			{
				$serviceRequest[$key]->driverCancelAmount = 0;
				$serviceRequest[$key]->passengerCancelAmount = 0;
				if($value->request_status!='CANCELBYDRIVER'){
					$TransactionLogService = new TransactionLogService;
					$LogService= $TransactionLogService->getPaidCancelAmount((object)['request_id'=>$value->request_id]);
					$serviceRequest[$key]->passengerCancelAmount = $LogService['data'];
				}
				if($value->request_status=='CANCELBYDRIVER'){
					$serviceRequest[$key]->driverCancelAmount = $value->cost;
				}
			}
			// dd($serviceRequest);
			return view('admin.transaction.driver-index',compact('serviceRequest','id'));

		} else {
			return redirect()->back()->with('flash_error', 'Transaction not found!');
		}
		return view('admin.transaction.driver-index');
	}

	public function transactionPayout(Request $request){

		$this->validate($request, [
			'comments'    => 'required',
			'transaction_id'    => 'required|unique:transaction_log,payment_gateway_transaction_id',
        ]);

		$TranLog=new TransactionLog();
		$TranLog->payment_gateway_transaction_id=$request->transaction_id;
		$TranLog->comments=$request->comments;
		$TranLog->is_paid = 'Y';
		$TranLog->status = 'COMPLETED';
		$TranLog->types = 'DEBIT';
		$TranLog->cost = $request->cost;
		$TranLog->transaction_type = 'PAYOUTFORRIDE';
		$TranLog->payout_user_id = $request->driver_id;
		$TranLog->save();
		$request_arr = explode(",",$request->request_id);
		foreach($request_arr as $value){
			$transaction = TransactionLog::where('request_id', $value)->first();
			$transaction->is_paid = 'Y';
			$transaction->payout_reference_id = $TranLog->transaction_log_id;
			$transaction->save();
		}

		return response()->json([
            "success" => true,
            "message" => "Payout inserted successfully"
        ],201);
	}

	public function passengerTransaction(Request $request,$id){
        $serviceRequest = DB::select('SELECT * from transaction_log as tl
		left join service_requests as sr on tl.request_id=sr.request_id
		where sr.passenger_id=? and tl.request_id<>"" and (tl.transaction_type IN ("FORRIDE","CANCELBYPASSENGERCHARGE")) order by sr.request_id DESC',[$request->id]);
		if(count($serviceRequest)>0){
			foreach($serviceRequest as $key=>$value)
			{
				$serviceRequest[$key]->driverCancelAmount = 0;
				$serviceRequest[$key]->passengerCancelAmount = 0;
				if($value->request_status=='CANCELBYPASSENGER'){
					$serviceRequest[$key]->passengerCancelAmount  = $value->cost;
				}
				if($value->request_status=='CANCELBYDRIVER'){
					$serviceRequest[$key]->driverCancelAmount = $value->cost;
				}
			}
           // echo '<pre>';print_r($serviceRequest); echo '<pre>';die;
			$passenger = PassengersProfile::where('user_id',$serviceRequest[0]->passenger_id)->first();
			return view('admin.transaction.passenger-index',compact('serviceRequest','passenger'));
		} else {
			return redirect()->back()->with('flash_error', 'Transaction not found!');
		}
		return view('admin.transaction.passenger-index',compact('id'));
		//return view('admin.transaction.passenger-index',compact('id'));
	}

	public function passengerTransactionList(Request $request)
	{
		try{
            $request->user_id=$request->passenger_id;
            $ServiceRequestService=new ServiceRequestService();
            $TransactionLogService=new TransactionLogService();
            $PaymentService= new PaymentService();
            $returnRequest=$ServiceRequestService->accessGetallRequestByCustomerPagewise($request);
            foreach($returnRequest["data"] as $key=>$val){
                $returnRequest["data"][$key]['created_at']=$this->checkNull($returnRequest["data"][$key]['created_at']);
                if($returnRequest["data"][$key]['created_at']!=""){
                    $returnRequest["data"][$key]['created_at']=$this->convertFromUTC($returnRequest["data"][$key]['created_at'],$request->timeZone);
                }
                $tracationsReturn=$TransactionLogService->getCreditNoteForRideForTripHistory((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
                // getPaidCancelAmount
                //accessGetCardDetailsByCardId

                $cancelAmount=$TransactionLogService->getPaidCancelAmount((object)['request_id'=>$returnRequest["data"][$key]['request_id']]);
               //print_r($cancelAmount); exit;

                $returnRequest["data"][$key]['transaction']=$tracationsReturn['data']['TransactionLog'];
                $returnRequest["data"][$key]['transaction']->cancelFee=(float)$cancelAmount['data'];
                $returnRequest["data"][$key]['transaction']->cost=(float)$returnRequest["data"][$key]['transaction']->cost-(float)$returnRequest["data"][$key]['transaction']->promo_code_value;//-(float)$cancelAmount;
                if($returnRequest["data"][$key]['transaction']->cost<=0){
                    $returnRequest["data"][$key]['transaction']->cost=0.00;
                }
                $returnRequest["data"][$key]['transaction']->cost= number_format((float)($returnRequest["data"][$key]['transaction']->cost), 2, '.', '');
                $returnRequest["data"][$key]['cardDetais']=(object)[];
                if($returnRequest["data"][$key]['payment_method']=="CARD" && $returnRequest["data"][$key]['card_id']!="" && $returnRequest["data"][$key]['card_id']!=null  ){

                    $cardDetails=$PaymentService->accessGetCardDetailsByCardId((object)["user_cards_id"=>$returnRequest["data"][$key]['card_id']]);
                    $returnRequest["data"][$key]['cardDetais']=$cardDetails['data'];
                }
            }
            return response(['message'=>"Default card updated","data"=>$returnRequest,"errors"=>array("exception"=>["Card Updated"],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
        catch(ModelNotFoundException $e){
            return response(['message'=>'Your not authorized to access',"errors"=>array("exception"=>["Invalid credentials"])],401);
        }
	}

	public function adminEarning(Request $request)
	{
		$serviceRequest = DB::select('SELECT * from transaction_log as tl
		left join service_requests as sr on tl.request_id=sr.request_id
		where  sr.request_id<>"" and (tl.transaction_type IN ("FORRIDE","CANCELBYPASSENGERCHARGE","CANCELBYDRIVERCHARGE")) order by sr.request_id DESC');
		if(count($serviceRequest)>0){
			foreach($serviceRequest as $key=>$value)
			{
				$serviceRequest[$key]->driverCancelAmount = 0;
				$serviceRequest[$key]->passengerCancelAmount = 0;
				if($value->request_status=='CANCELBYPASSENGER'){
					$serviceRequest[$key]->passengerCancelAmount  = $value->cost;
				}
				if($value->request_status=='CANCELBYDRIVER'){
					$serviceRequest[$key]->driverCancelAmount = $value->cost;
				}
			}
			return view('admin.transaction.admin-earning',compact('serviceRequest'));

		} else {
			return redirect()->back()->with('flash_error', 'Transaction not found!');
		}
		return view('admin.transaction.admin-earning');
	}

}
