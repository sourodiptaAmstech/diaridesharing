<?php

namespace App\Http\Controllers\Admin\CancelCharge;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Request\ServiceRequest;
use App\Model\Transaction\TransactionLog;

class CancelChargeController extends Controller
{
    public function passengerCancelReport(){
        $passengerReports = TransactionLog::join('service_requests as sr','sr.request_id','=','transaction_log.request_id')
                    ->join('passengers_profile as pp','pp.user_id','=','sr.passenger_id')
                    ->select('transaction_log.is_paid','transaction_log.status','transaction_log.cost','transaction_log.currency','transaction_log.transaction_type','sr.*','pp.first_name','pp.last_name')
                    ->where('transaction_log.transaction_type', 'CANCELBYPASSENGERCHARGE')
                    ->get();
        return view('admin.cancelReport.passenger-cancel',compact('passengerReports'));
    }

    public function driverCancelReport(){
        $driverReports = TransactionLog::join('service_requests as sr','sr.request_id','=','transaction_log.request_id')
                    ->join('drivers_profile as dp','sr.driver_id','=','dp.user_id')
                    ->select('transaction_log.is_paid','transaction_log.status','transaction_log.cost','transaction_log.currency','transaction_log.transaction_type','sr.*','dp.first_name','dp.last_name')
                    ->where('transaction_log.transaction_type', 'CANCELBYDRIVERCHARGE')
                    ->get();
        return view('admin.cancelReport.driver-cancel',compact('driverReports'));
    }
}
