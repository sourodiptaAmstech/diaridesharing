<?php
namespace App\Http\Controllers\Api\Driver;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\AuthService;
use Illuminate\Support\Facades\Auth;
use App\Services\DriversProfileService;
use App\Services\EmergencyContact;
use App\Services\UsersDevices;
use App\Http\Controllers\Api\Driver\RegistrationController;
use App\Services\DriversServiceType;
use Validator;

class AuthController extends Controller
{
    public function login(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'social_unique_id' => ['required_if:login_by,facebook,google,apple'],
                'username'=>'required_if:login_by,manual',
                'password' => 'required_if:login_by,manual|min:6',
                'timeZone'=>'required',
                'device_type' => 'required|in:web,android,ios',
                'device_token' => 'required',
                'device_id' => 'required',
                'login_by' => 'required|in:manual,facebook,google,apple',
                'isd_code'=>'required_if:login_by,manual'
            ];

            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="driver-service";
            $AuthService=new AuthService();
            $Profile=new DriversProfileService();
            $UserDevice=new UsersDevices();
            $DriversServiceType=new DriversServiceType();
          //  $EmergencyContact=new EmergencyContact();
          $profile=$Profile->accessGetLoginWithPhoneNo($request);
          $request->user_id=-1;
          if($profile['statusCode']==200){
            $request->user_id=$profile['data']->user_id;
          }
            $AuthData=[];
            $AuthData=$AuthService->accessLogin($request);
            if($AuthData['statusCode']==200){
                $UserAccessToken=$AuthData['data']->access_token;
                $token_type=$AuthData['data']->token_type;
                $request->user_id=$AuthData['data']->user_id;

                $ProfileData=$Profile->accessGetProfile($request);
                $DevicesData=$UserDevice->accessUpdateDevices($request);
                $DriversServiceTypeData=$DriversServiceType->accessGetService($request);
               // $EmergencyContactData=$EmergencyContact->accessGetContact($request);

                // check for the mobile is verifived of not

                if((int)$ProfileData['data']->isMobileverified===0){
                    // logout the loged in user and login the user with temp accesss
                    $UserToken=$AuthService->reLogin((object)['user_id'=>$request->user_id,"user_scope"=>"temporary-customer-service"]);
                    $UserAccessToken=$UserToken['data']->access_token;
                    $token_type=$UserToken['data']->token_type;
                    $request->user_id=$AuthData['data']->user_id;

                    //logut all access user
                   $AuthService->accesslogoutTempUser((object)['user_id'=>$request->user_id,"user_scope"=>"driver-service"]);;
                }
            return response(['message'=>trans("api.SYSTEM_MESSAGE.LOGGED_IN"),"data"=>(object)["access_token"=>$UserAccessToken,"token_type"=>$token_type,"user_profile"=>$Profile->setProfileData($ProfileData['data'],$request->login_by),/*"emergency_contacts"=>$EmergencyContactData*/"driver_services"=>$DriversServiceTypeData['data']],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);
            }
            else if($AuthData['statusCode']==401 && ($request->login_by=="facebook" || $request->login_by=="google" || $request->login_by=="apple")){
                $RegistrationController=new RegistrationController();
                $RegistrationSocial=$RegistrationController->register($request);
                return $RegistrationSocial;
            }
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }

    }
    public function logout(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="driver-service";
            $AuthService=new AuthService();
            $AuthData=$AuthService->accessLogout($request);
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);
        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function forgetPassword(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'mobile_no' => 'required|max:10',
                'isdCode'=>'required',
                'timeZone'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="driver-service";
            $AuthService=new AuthService();
            $AuthData=$AuthService->accessForgetPasswordDriver($request);
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    public function resetPassword(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'mobile_no' => 'required|max:10',
                'isdCode'=>'required',
                'timeZone'=>'required',
                'password' => 'required|between:6,255|confirmed'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };
            $request->user_scope="driver-service";
            $AuthService=new AuthService();
            $AuthData=$AuthService->accessResetPasswordDriver($request);
            return response(['message'=>$AuthData['message'],"data"=>$AuthData['data'],"errors"=>$AuthData['errors']],$AuthData['statusCode']);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }


}
