<?php

namespace App\Http\Controllers\Api\TermCondition;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Services\SettingServices;
use App\Model\Contact\ContactUs;
use App\Notifications\SOSNotification;
use App\Notifications\ContactusNotification;
use Illuminate\Support\Facades\Notification;
use App\User;
use App\Model\Profiles\PassengersProfile;
use App\Model\Profiles\DriverProfiles;
use App\Model\Request\SOSDetail;
use Illuminate\Support\Facades\Auth;


class TermConditionController extends Controller
{
    public function getTermCondition(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'languageParam'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            if ($request->languageParam=='en') {
            	$key = 'condition_privacy';
            }

            if ($request->languageParam=='pt') {
            	$key = 'condition_privacy_pt';
            }

            $settingServices=new SettingServices();
            $settingServices=$settingServices->getValueByKey($key);


            return response(['message'=>"Terms & Conditions","data"=>$settingServices,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function getTermConditionDriver(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'languageParam'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            if ($request->languageParam=='en') {
            	$key = 'condition_privacy_driver';
            }

            if ($request->languageParam=='pt') {
            	$key = 'condition_privacy_driver_pt';
            }

            $settingServices=new SettingServices();
            $settingServices=$settingServices->getValueByKey($key);

            return response(['message'=>"Terms & Conditions","data"=>$settingServices,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);
        }
        catch(\Illuminate\Database\QueryException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    //get passenger's privacy policy
    public function getPrivacyPolicy(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'languageParam'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            if ($request->languageParam=='en') {
            	$key = 'page_privacy';
            }

            if ($request->languageParam=='pt') {
            	$key = 'page_privacy_pt';
            }

            $settingServices=new SettingServices();
            $settingServices=$settingServices->getValueByKey($key);

            return response(['message'=>"Privacy Policy","data"=>$settingServices,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }
    //get driver's privacy policy
    public function getPrivacyPolicyDriver(Request $request){
        try{
            $request['timeZone']=$timeZone=$request->header("timeZone");
            $rule=[
                'timeZone'=>'required',
                'languageParam'=>'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.VALIDATION_FAILED")],"e"=>[])],422); };

            if ($request->languageParam=='en') {
                $key = 'page_privacy_driver';
            }

            if ($request->languageParam=='pt') {
                $key = 'page_privacy_driver_pt';
            }

            $settingServices=new SettingServices();
            $settingServices=$settingServices->getValueByKey($key);

            return response(['message'=>"Privacy Policy","data"=>$settingServices,"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.WORKING_AS_EXPECTED")],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.DATABASE_EXCEPTION")],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG"),"data"=>(object)[],"errors"=>array("exception"=>[trans("api.SYSTEM_MESSAGE.SOMETHING_WENT_WRONG")])],400);
        }
    }

    public function contactUS(Request $request){
        try{

            $rule=[
                'name'      => 'required|max:255',
                'email'     => 'required|email|max:255',
                'phone_no'  => 'required',
                'address'   => 'required',
                'user_type' => 'required|in:passenger,driver'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $ContactUs = new ContactUs();
            $ContactUs->name=$request->name;
            $ContactUs->email=$request->email;
            $ContactUs->phone_no=$request->phone_no;
            $ContactUs->address=$request->address;
            $ContactUs->user_type=$request->user_type;
            $ContactUs->save();
           
            $msg1 = $ContactUs->name.' trying to contact you.';
            $msg2 = 'Contact No.: '.$ContactUs->phone_no;
            $msg3 = 'Email ID: '.$ContactUs->email;
            $msg4 = 'Address: '.$ContactUs->address;

            $sub = 'Someone Contact You';
            $user = User::where('user_scope','admin-service')->first();

            $user->email = $user->username;
            
            Notification::send($user, new ContactusNotification($msg1,$msg2,$msg3,$msg4,$sub));
            //return response(['message'=>"Contact us.","data"=>$ContactUs,"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);
            echo "1"; exit();

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

    public function sosEmail(Request $request){
        try{
            $rule=[
                'request_id' => 'required',
                'longitude' => 'required',
                'latitude' => 'required',
                'current_address'   => 'required',
                'user_type' => 'required'
            ];
            $validator=$this->requestValidation($request->all(),$rule);
            if($validator->status=="false"){ return response(['message'=>$validator->message,"field"=>$validator->field,"data"=>(object)[],"errors"=>array("exception"=>["Request Validation Failed"],"e"=>[])],422); };

            $request->user_id=Auth::user()->id;

            $SOSDetail = SOSDetail::where('user_id',$request->user_id)->where('request_id',$request->request_id)->first();

            if (isset($SOSDetail)) {
                $SOSDetail->longitude = $request->longitude;
                $SOSDetail->latitude = $request->latitude;
                $SOSDetail->current_address = $request->current_address;
                $SOSDetail->save();
            } else {
                $SOSDetail = new SOSDetail();
                $SOSDetail->user_id = $request->user_id;
                $SOSDetail->request_id = $request->request_id;
                $SOSDetail->longitude = $request->longitude;
                $SOSDetail->latitude = $request->latitude;
                $SOSDetail->current_address = $request->current_address;
                $SOSDetail->user_type = $request->user_type;
                $SOSDetail->save();
            }
            if ($request->user_type=='passenger') {
                $PassengersProfile = PassengersProfile::where('user_id',$request->user_id)->first();
            }
            if ($request->user_type=='driver') {
                $PassengersProfile = DriverProfiles::where('user_id',$request->user_id)->first();
            }
            $settingServices=new SettingServices();
            $sosNumber=$settingServices->getValueByKey("sos_number");

            $msg1 = $PassengersProfile->first_name.' '.$PassengersProfile->last_name.' trying to contact you.';
            $msg2 = 'Contact No.: '.$PassengersProfile->isd_code.'-'.$PassengersProfile->mobile_no;
            $msg3 = 'Email ID: '.$PassengersProfile->email_id;
            $msg4 = 'Current Address: '.$request->current_address;
            $msg5 = 'Latitude: '.$request->latitude;
            $msg6 = 'Longitude: '.$request->longitude;
            $msg7 = 'SOS number: '.$sosNumber;

            $sub = 'Dia Contact';
            $user = User::where('user_scope','admin-service')->first();

            $settingServices=$settingServices->getValueByKey("sos_email");
            $emails = json_decode($settingServices);
            
            foreach ($emails as $email) {
                $user->email = $email;
                Notification::send($user, new SOSNotification($msg1,$msg2,$msg3,$msg4,$msg5,$msg6,$msg7,$sub));
            }
            return response(['message'=>trans("api.SYSTEM_MESSAGE.SOSEMAIL"),"data"=>[],"errors"=>array("exception"=>["Everything is OK."],"e"=>[])],200);

        }
        catch(\Illuminate\Database\QueryException  $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["DataBase Excetion"],"e"=>$e)],500);
        }
        catch(\Illuminate\Http\Exceptions\HttpResponseException $e){
            return response(['message'=>"Something went wrong","data"=>(object)[],"errors"=>array("exception"=>["Http Response Exception: Bad Request"])],400);
        }
    }

}
