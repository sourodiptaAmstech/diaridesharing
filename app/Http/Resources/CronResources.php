<?php


namespace App\Http\Resources;


use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Api\Passenger\RequestController;
use Illuminate\Support\Facades\Log;

/**
 * Class CronResources
 * @package App\Http\Resources
 */
class CronResources
{
    public function __invoke()
    {
        $this->lockCronService();
        $this->scheduleRide();
        $this->unLockCronService();

        return null;
    }
    private function lockCronService(){
        DB::table('cron_lock')->where('id',1)->update(array(
            'status'=>1,
        ));
    }
    private function unLockCronService(){
        DB::table('cron_lock')->where('id',1)->update(array(
            'status'=>0,
        ));
    }
    private function scheduleRide(){
       try{
           $RequestRide=new RequestController();
           $RequestRide->sendScheduleRideNotification();
       }
       catch (\Exception $e){

       }
    }


}
